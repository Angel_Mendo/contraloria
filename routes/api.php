<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Yajra\DataTables\Html\Builder;

use App\Http\Controllers\API\APICatalogController;
use App\Http\Controllers\API\APIReportController;
use App\Http\Controllers\API\APIAuth;

use App\Http\Controllers\API\APIChartController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'catalog/'], function () {
    Route::any('/mechanism/list', function (Request $request, Builder $builder) {
        return (new APICatalogController())->mechanismList($request, $builder);
    })->name('api.catalog.mechanism.list');

    Route::any('/area/list', function (Request $request) {
        return (new APICatalogController())->areaList($request);
    })->name('api.catalog.area.list');

    Route::any('/sanction/list', function (Request $request) {
        return (new APICatalogController())->sanctionList($request);
    })->name('api.catalog.sanction.list');

    Route::any('/cause/list', function (Request $request) {
        return (new APICatalogController())->causeList($request);
    })->name('api.catalog.cause.list');

    Route::any('/dependence/list', function (Request $request) {
        return (new APICatalogController())->dependenceList($request);
    })->name('api.catalog.dependence.list');
});


Route::group(['prefix' => 'report/'], function () {
    Route::any('/list', function (Request $request) {
        return (new APIReportController())->reportList($request);
    })->name('api.report.list');
});

Route::group(['prefix' => 'user/'], function () {
    Route::any('/list', function (Request $request) {
        return (new APIAuth())->userList($request);
    })->name('api.user.list');
});


Route::group(['prefix' => 'chart/'], function () {
    Route::any('/chart_report', function (Request $request) {
        return (new APIChartController())->chart_report($request);
    })->name('api.chart.chart_report');

    Route::any('/informer_gender', function (Request $request) {
        return (new APIChartController())->chart_informer_gender($request);
    })->name('api.chart.informer_gender');

    Route::any('/informer_type', function (Request $request) {
        return (new APIChartController())->chart_informer_type($request);
    })->name('api.chart.informer_type');


    Route::any('/report_type', function (Request $request) {
        return (new APIChartController())->chart_report_type($request);
    })->name('api.chart.report_type');


    Route::any('/informer_mechanism', function (Request $request) {
        return (new APIChartController())->chart_informer_reception($request);
    })->name('api.chart.mechanism');


    Route::any('/informer_status', function (Request $request) {
        return (new APIChartController())->chart_informer_status($request);
    })->name('api.chart.status');


    Route::any('/informer_dependency', function (Request $request) {
        return (new APIChartController())->chart_informer_dependency($request);
    })->name('api.chart.dependency');

    Route::any('/informer_unit_processing', function (Request $request) {
        return (new APIChartController())->chart_unit_processing($request);
    })->name('api.chart.unit_processing');



    Route::any('/survey', function (Request $request) {
        return (new APIChartController())->chart_suverv($request);
    })->name('api.chart.suvery');

});

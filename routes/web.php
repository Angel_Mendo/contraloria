<?php

use App\Models\SubstanceUnit;
use Illuminate\Http\Request;
use App\Http\Middleware\CheckSession;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\Utils;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
})->name('landing');


Route::get('/estadisticas', function () {
    $report = count(\App\Models\Report::all());
    $report_unit = count(\App\Models\ResearchUnit::all());
    $report_solving = count(\App\Models\SolvingUnit::all());
    $report_substance = count(\App\Models\SubstanceUnit::all());

    $total = $report - $report_unit - $report_solving - $report_substance;
    return view('estadisticas')->with('report', $report)->with('report_unit', $report_unit)
        ->with('report_solving', $report_solving)->with('report_substance', $report_substance)
        ->with('total', $total);

})->name('estadisticas');

Route::get('/estadisticas_sexo', function () {
    return view('estadisticas_sexo');
})->name('estadisticas_sexo');


Route::get('/estadisticas_mecanismo', function () {
    return view('estadisticas_mecanismos');
})->name('estadisticas_mecanismo');


Route::get('/estadisticas_dependencia', function () {
    return view('estadisticas_dependencia');
})->name('estadisticas_dependencia');

Route::get('/estadisticas_estatus', function () {
    return view('estadisticas_estatus');
})->name('estadisticas_estatus');

Route::get('/estadisticas_tipo', function () {
    return view('estadisticas_tipo');
})->name('estadisticas_tipo');


Route::get('/estadisticas_tipo_tramite', function () {
    return view('estadisticas_tipo_tramite');
})->name('estadisticas_tipo_tramite');

Route::get('/estadisticas_calificacion', function () {
    return view('estadisticas_calificacion');
})->name('estadisticas_calificacion');


Route::get('/estadisticas_resolucion', function () {
    return view('estadisticas_resolucion');
})->name('estadisticas_resolucion');



Route::get('/politicas', function () {
    return view('politicas');
})->name('politicas');


Route::group(['prefix' => 'denuncia/'], function () {
    Route::any('/registro', function (Request $request) {
        return (new ReportController())->denuncia($request);
    })->name('denuncia.registro');

//    Encuesta
    Route::any('/encuesta/satisfacción/{pk}', function (Request $request, $pk) {
        return (new ReportController())->encuesta_satisfaccion($request, $pk);
    })->name('encuesta.satisfaccion');

    Route::any('/registrada/{pk}', function (Request $request, $pk) {
        return (new ReportController())->denuncia_registrada($request, $pk);
    })->name('denuncia.registrada');

    Route::any('/seguimiento', function (Request $request) {
        return (new ReportController())->denuncia_seguimiento($request);
    })->name('denuncia.seguimiento');

    Route::any('/perfil_denuncia/{pk}', function (Request $request, $pk) {
        return (new ReportController())->perfil_denunica($request, $pk);
    })->name('denuncia.perfil');


});

Route::group(['prefix' => 'admin/'], function () {

    Route::any('/delete/object/{model}/{pk}', function (Request $request, $model, $pk) {
        return (new Utils())->delete_object($request, $model, $pk);
    })->name('util.delete.object');

    Route::any('/', function (Request $request) {
        return (new LoginController())->login($request);
    })->name('admin');

    Route::any('/login', function (Request $request) {
        return (new LoginController())->login($request);
    })->name('admin.login');

    Route::any('/logout', function () {
        return (new LoginController())->logout();
    })->name('admin.logout');


    Route::any('/estadisticas', function (Request $request) {
        return (new DashboardController())->stats($request);
    })->name('dashboard.index')->middleware(CheckSession::class);


    Route::any('/dashboard', function (Request $request) {
        return (new DashboardController())->dashboard($request);
    })->name('dashboard.dashboard')->middleware(CheckSession::class);


    Route::group(['prefix' => 'report/'], function () {
        Route::any('/list', function (Request $request) {
            return (new ReportController())->list($request);
        })->name('report.list')->middleware(CheckSession::class);

        Route::any('/register/{pk}/{step}', function (Request $request, $pk, $step) {
            return (new ReportController())->register($request, $pk, $step);
        })->name('report.register')->middleware(CheckSession::class);

        Route::any('/dowload', function (Request $request) {
            return (new ReportController())->download($request);
        })->name('report.download')->middleware(CheckSession::class);

        Route::any('/upload', function (Request $request) {
            return (new ReportController())->upload($request);
        })->name('report.upload')->middleware(CheckSession::class);


        Route::any('/file', function (Request $request) {
            return (new ReportController())->file($request);
        })->name('report.file')->middleware(CheckSession::class);
    });

//    Usuarios
    Route::group(['prefix' => 'user/'], function () {
        Route::any('/list', function (Request $request) {
            return (new RegisterController())->list($request);
        })->name('user.list')->middleware(CheckSession::class);

        Route::any('/register/{pk}', function (Request $request, $pk) {
            return (new RegisterController())->resgisterUser($request, $pk);
        })->name('user.register')->middleware(CheckSession::class);
    });


//    Catalogos
    Route::group(['prefix' => 'catalog/'], function () {
        Route::any('/area', function (Request $request) {
            return (new CatalogController())->areaList($request);
        })->name('catalog.area.list')->middleware(CheckSession::class);

        Route::any('/mechanism', function (Request $request) {
            return (new CatalogController())->receptionMechanismList($request);
        })->name('catalog.mechanism.list')->middleware(CheckSession::class);

        Route::any('/sanction', function (Request $request) {
            return (new CatalogController())->sanctionList($request);
        })->name('catalog.sanction.list')->middleware(CheckSession::class);

        Route::any('/cause', function (Request $request) {
            return (new CatalogController())->causeList($request);
        })->name('catalog.cause.list')->middleware(CheckSession::class);

        Route::any('/dependence', function (Request $request) {
            return (new CatalogController())->dependenceList($request);
        })->name('catalog.dependence.list')->middleware(CheckSession::class);
    });
});

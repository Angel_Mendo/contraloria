<?php

namespace App\Imports;

use App\Models\ReceptionMechanism;
use App\Models\Report;
use App\Models\ResearchUnit;
use App\Models\SolvingUnit;
use App\Models\SubstanceUnit;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class ReportImport implements ToCollection, WithProgressBar
{
    use Importable;

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            if (intval($row[0]) and $row[1]) {
                $mechanism = ReceptionMechanism::getOrNone('name', $row[6]);

                $data = array(
                    'mechanism_id' => ($mechanism) ? $mechanism->id : null,
                    'exercise' => $row[2],
                    'update_period' => date('Y-m-d 00:00:00', strtotime($row[3])),
                    'reception_date' => date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[4]))),
                    'report_no' => $row[5],
                    'report_type' => array_search($row[13], Report::REPORT_TYPE),
                    'informer_type' => array_search($row[7], Report::INFORMER_TYPE),
                    'informer_gender' => array_search($row[8], Report::GENDER),
                    'informer_name' => $row[9],
                    'accused_name' => $row[10],
                    'accused_dependency' => $row[11],
                    'accused_charge' => $row[12],
                    'reported_event' => $row[14],
                    'attention_period' => (intval($row[15])) ? $row[15] : 0,
                    'unit_processing' => array_search($row[16], Report::UNIT_PROCESSING),
                    'status' => array_search($row[17], Report::STATUS),
                    'status_date' => date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[18]))),
                    'commentary' => $row[19],
                );
                $object = Report::getOrNone('report_no', $row[5]);
                $object->fill($data)->save();
                $object_id = $object->id;

                if ($row[20] and $row[21]) {
                    $object = ResearchUnit::getOrNone('report_id', $object_id);

                    $data = array(
                        'report_id' => $object_id,
                        'research_no' => $row[20],
                        'reception_date' => date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[21]))),
                        'settlement_date' => (strtotime($row[22])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[22]))) : null,
                        'research_origin' => array_search($row[23], ResearchUnit::RESEARCH_ORIGIN),
                        'investigated_event' => $row[24],
                        'responsible_lawyer' => $row[25],
                        'research_status' => array_search($row[26], ResearchUnit::RESEARCH_STATUS),
                        'status_date' => date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[29]))),
                        'qualification' => (intval($row[27])) ? $row[27] : null,
                        'qualification_date' => (strtotime($row[28])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[28]))) : null,
                        'challenge_date' => (strtotime($row[30])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[30]))) : null,
                        'note' => $row[31]
                    );
                    $object->fill($data)->save();
                }

                if ($row[32] and $row[33]) {
                    $object = SubstanceUnit::getOrNone('report_id', $object_id);

                    $data = array(
                        'report_id' => $object_id,
                        'reception_date' => date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[32]))),
                        'status' => array_search($row[33], SubstanceUnit::SUBSTANCE_STATUS),
                        'admission_date' => (strtotime($row[34])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[34]))) : null,
                        'process_no' => $row[35],
                        'type_foul' => array_search($row[36], SubstanceUnit::TYPE_FOUL),
                        'presumed_responsible' => $row[37],
                        'presumed_responsible_gender' => array_search($row[38], Report::GENDER_DENUNCIADO),
                        'responsible_lawyer' => $row[39],
                        'agreement_date' => (strtotime($row[40])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[40]))) : null,
                        'emplacement_date' => (strtotime($row[41])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[41]))) : null,
                        'audience_initial_date' => (strtotime($row[42])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[42]))) : null,
                        'tja_date' => (strtotime($row[43])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[43]))) : null,
                        'tja_no' => $row[44],
                        'tja_room' => $row[45],
                        'test_date' => (strtotime($row[46])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[46]))) : null,
                        'allegation_date' => (strtotime($row[47])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[47]))) : null,
                        'resolution_area_date' => (strtotime($row[48])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[48]))) : null,
                        'note' => $row[49]
                    );
                    $object->fill($data)->save();
                }

                if ($row[50] and $row[51] and $row[52]) {
                    $object = SolvingUnit::getOrNone('report_id', $object_id);

                    $data = array(
                        'report_id' => $object_id,
                        'analysis' => array_search($row[50], SolvingUnit::ANALYSIS_PRA),
                        'closing_date' => date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[51]))),
                        'resolution_date' => date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[52]))),
                        'resolution_type' => array_search($row[53], SolvingUnit::RESOLUTION_TYPE),
                        'sanction_type' => array_search($row[54], SolvingUnit::SANCTION_TYPE),
                        'sanction_description' => $row[55],
                        'article' => $row[56],
                        'sanctioned_person' => $row[57],
                        'sanctioned_gender' => array_search($row[58], Report::GENDER),
                        'notification_date' => (strtotime($row[59])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[59]))) : null,
                        'exec_report_date' => (strtotime($row[60])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[60]))) : null,
                        'causing_date' => (strtotime($row[61])) ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $row[61]))) : null,
                        'note' => $row[62]
                    );
                    $object->fill($data)->save();
                }

//                Log::debug($object->id);
//                Log::debug('-----------------------------');
            }
        }
    }
}

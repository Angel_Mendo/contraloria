<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function list()
    {
        return view('user.list');
    }

    public function resgisterUser(Request $request, $id)
    {
        $response = null;
        $error_msg = null;

        $user = Auth::user();
        $object = User::findOrCreate($id);
        if ($request->method() == 'POST') {
            $save = true;

            $object->name = $request->input('name');
            $object->role = $request->input('role');
            $object->is_supervisor = $request->input('is_supervisor');

            $password = $request->input('password');
            $password2 = $request->input('password2');

            if ($password != $password2) {
                $response = array(
                    'status' => 'error',
                    'message' => 'Las contraseñas no coinciden.'
                );

                return view('user.form')
                    ->with('response', $response)
                    ->with('object', $object);
            }

            if (!$object->id and empty($password)) {
                $response = array(
                    'status' => 'error',
                    'message' => 'Ingresa una contraseña.'
                );

                return view('user.form')
                    ->with('response', $response)
                    ->with('object', $object);
            }

            if (!$object->id or !empty($password)) {
                $object->password = Hash::make($password);
            }

            $email_str = $request->input('email');
            if (!$object->id or $object->email != $email_str) {
                $email = User::where('email', $email_str)->first();
                if (!$email) {
                    $object->email = $email_str;
                } else {
                    $response = array(
                        'status' => 'error',
                        'message' => 'El email ya existe en el sistema.'
                    );

                    return view('user.form')
                        ->with('response', $response)
                        ->with('object', $object);
                }
            }

            $response = array(
                'success' => 'Guardado correctamente.'
            );
            $object->save();
//            return response($object, 200);
        }

        return view('user.form')
            ->with('message', $response)
            ->with('object', $object);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $object)
    {
        if (!$object->id) {
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
            ]);
        }

        $array_response = array(
            'customMessages' => [],
            'fallbackMessages' => [],
            'customAttributes' => [],
            'customValues' => [],
            'extensions' => [],
            'replacers' => [],
            'fails' => []
        );

        return $array_response;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Http\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function __construct()
    {
    }

    public function login(Request $request)
    {
        if ($request->method() == 'GET') {
            if (Session::has('pk')) {
                return redirect()->route('dashboard.dashboard');
            } else {
                return view('login.login');
            }
        } elseif ($request->method() == 'POST') {
            $userdata = array(
                'email' => $request->input('email'),
                'password' => $request->input('password')
            );

//            if (Auth::attempt($userdata, $request->input('remember-me', 0)) && is_null(Auth::user()->deleted_at)) {
            if (Auth::attempt($userdata, $request->input('remember-me', 1)) && is_null(Auth::user()->deleted_at)) {
                session(['pk' => Auth::user()->id, 'full_name' => Auth::user()->name, 'id' => Auth::user()->id]);
                return redirect()->route('dashboard.dashboard');
            }

            $response = array(
                'status' => 'conflict',
                'message' => 'Por favor revisa tu usuario y contraseña.',
            );
            return view('login.login')->with('message', $response);
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('admin.login');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Cause;
use App\Models\Dependence;
use App\Models\ReceptionMechanism;
use App\Models\Sanction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CatalogController extends Controller
{
    public function areaList(Request $request)
    {
        $response = [];
        $pk = $request->input('pk') ? $request->input('pk') : 0;
        $object = Area::findOrCreate($pk);
        if ($request->method() == 'POST') {

            $request['code'] = Utils::generateSlug($request->name);

            $object->fill($request->toArray())->save();

            $response['success'] = 'Guardado correctamente.';
        }

        return view('catalog.area')
            ->with('object', $object)
            ->with('message', $response);
    }

    public function sanctionList(Request $request)
    {
        $response = [];
        $pk = $request->input('pk') ? $request->input('pk') : 0;
        $object = Sanction::findOrCreate($pk);
        if ($request->method() == 'POST') {

            $request['code'] = Utils::generateSlug($request->name);

            $object->fill($request->toArray())->save();

            $response['success'] = 'Guardado correctamente.';
        }

        return view('catalog.sanction')
            ->with('object', $object)
            ->with('message', $response);
    }

    public function receptionMechanismList(Request $request)
    {
        $response = [];
        $pk = $request->input('pk') ? $request->input('pk') : 0;
        $object = ReceptionMechanism::findOrCreate($pk);
        if ($request->method() == 'POST') {

            $request['code'] = Utils::generateSlug($request->name);

            $object->fill($request->toArray())->save();

            $response['success'] = 'Guardado correctamente.';
        }

        return view('catalog.mechanism')
            ->with('object', $object)
            ->with('message', $response);
    }

    public function causeList(Request $request)
    {
        $response = [];
        $pk = $request->input('pk') ? $request->input('pk') : 0;
        $object = Cause::findOrCreate($pk);
        if ($request->method() == 'POST') {

            $request['code'] = Utils::generateSlug($request->name);

            $object->fill($request->toArray())->save();

            $response['success'] = 'Guardado correctamente.';
        }

        return view('catalog.cause')
            ->with('object', $object)
            ->with('message', $response);
    }

    public function dependenceList(Request $request)
    {
        $response = [];
        $pk = $request->input('pk') ? $request->input('pk') : 0;
        $object = Dependence::findOrCreate($pk);
        if ($request->method() == 'POST') {

            $request['code'] = Utils::generateSlug($request->name);

            $object->fill($request->toArray())->save();

            $response['success'] = 'Guardado correctamente.';
        }

        return view('catalog.dependence')
            ->with('object', $object)
            ->with('message', $response);
    }
}

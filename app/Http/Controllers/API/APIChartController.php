<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dependence;
use App\Models\ReceptionMechanism;
use App\Models\Report;
use App\Models\ResearchUnit;
use App\Models\SolvingUnit;
use App\Models\SubstanceUnit;
use App\Models\Suvery;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class APIChartController extends Controller
{
    public function chart_report(Request $request)
    {

        $series = array();
        $labels = array();

        $start = new Carbon($request->input('start'));
        $end = new Carbon($request->input('end'));

        while ($start->lte($end)) {
            $categories[] = $start->toDateString();
            $reports = Report::whereDate('reception_date', $start->toDateString());
            $data[] = $reports->count();

            $reports = ResearchUnit::whereDate('reception_date', $start->toDateString());
            $unit_research[] = $reports->count();

            $reports = SubstanceUnit::whereDate('reception_date', $start->toDateString());
            $unit_substance[] = $reports->count();

            $reports = SolvingUnit::whereDate('resolution_date', $start->toDateString());
            $unit_solving[] = $reports->count();

            $start->addDay();
        }

        $series[] = array(
            'name' => 'Recibidas',
            'data' => $data
        );

        $series[] = array(
            'name' => 'Unidad de Investigacion',
            'data' => $unit_research
        );

        $series[] = array(
            'name' => 'Unidad de Substanciación',
            'data' => $unit_substance
        );

        $series[] = array(
            'name' => 'Unidad de Resolucion',
            'data' => $unit_solving
        );

        $data = array(
            'series' => $series,
            'categories' => $categories,
            'type' => 'line',
            'title' => 'Denuncias',
        );

        return response()->json($data, 200);
    }

    public function chart_informer_gender(Request $request)
    {

        $series = array();
        $labels = array();

        $start = $request->input('start') . ' 00:00:00';
        $end = $request->input('end') . ' 23:59:00';

        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';
        foreach (Report::GENDER as $index => $gender) {
            if ($index != '') {


                $reports = Report::whereBetween('reception_date', array($start, $end))->where('informer_gender', $index)->count();

                array_push($labels, $gender);
                array_push($series, $reports);
            }
        }

        $data = array(
            'series' => $series,
            'labels' => $labels,

        );

        return response()->json($data, 200);
    }

    public function chart_informer_type(Request $request)
    {

        $series = array();
        $labels = array();

        $start = $request->input('start') . ' 00:00:00';
        $end = $request->input('end') . ' 23:59:00';

        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';
        foreach (Report::INFORMER_TYPE as $index => $value) {
            $reports = Report::whereBetween('reception_date', array($start, $end))->where('informer_type', $index)->count();

            array_push($labels, $value);
            array_push($series, $reports);
        }

        $data = array(
            'series' => $series,
            'labels' => $labels
        );

        return response()->json($data, 200);
    }


    public function chart_report_type(Request $request)
    {

        $series = array();
        $labels = array();

        $start = $request->input('start') . ' 00:00:00';
        $end = $request->input('end') . ' 23:59:00';

        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';
        foreach (Report::REPORT_TYPE as $index => $value) {
            $reports = Report::whereBetween('reception_date', array($start, $end))->where('report_type', $index)->count();

            array_push($labels, $value);
            array_push($series, $reports);
        }

        $data = array(
            'series' => $series,
            'labels' => $labels
        );

        return response()->json($data, 200);
    }

    public function chart_unit_processing(Request $request)
    {

        $series = array();
        $labels = array();

        $start = $request->input('start') . ' 00:00:00';
        $end = $request->input('end') . ' 23:59:00';

        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';
        foreach (Report::UNIT_PROCESSING as $index => $value) {
            $reports = Report::whereBetween('reception_date', array($start, $end))->where('unit_processing', $index)->count();

            array_push($labels, $value);
            array_push($series, $reports);
        }

        $data = array(
            'series' => $series,
            'labels' => $labels
        );

        return response()->json($data, 200);
    }

    public function chart_informer_reception(Request $request)
    {

        $series = array();
        $labels = array();

//        $start = $request->input('start') . ' 00:00:00';
//        $end = $request->input('end') . ' 23:59:00';


        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';

        foreach (ReceptionMechanism::all() as $index => $mechanism) {
            if ($index != '') {

                $reports = Report::whereBetween('reception_date', array($start, $end))->where('mechanism_id', $index)->count();

                array_push($labels, $mechanism->name);
                array_push($series, $reports);
            }
        }

        $data = array(
            'series' => $series,
            'labels' => $labels
        );

        return response()->json($data, 200);
    }

    public function chart_informer_dependency(Request $request)
    {

        $series = array();
        $labels = array();

//        $start = $request->input('start') . ' 00:00:00';
//        $end = $request->input('end') . ' 23:59:00';


        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';

        foreach (Dependence::all() as $index => $dependency) {
            if ($index != '') {

                $reports = Report::whereBetween('reception_date', array($start, $end))->where('accused_dependency', $index)->count();

                array_push($labels, $dependency->name);
                array_push($series, $reports);
            }
        }

        $data = array(
            'series' => $series,
            'labels' => $labels
        );

        return response()->json($data, 200);
    }

    public function chart_informer_status(Request $request)
    {

        $series = array();
        $labels = array();

//        $start = $request->input('start') . ' 00:00:00';
//        $end = $request->input('end') . ' 23:59:00';


        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';

        foreach (Report::STATUS as $index => $status) {
            if ($index != '') {

                $reports = Report::whereBetween('reception_date', array($start, $end))->where('STATUS', $index)->count();

                array_push($labels, $status);
                array_push($series, $reports);
            }
        }

        $data = array(
            'series' => $series,
            'labels' => $labels
        );

        return response()->json($data, 200);
    }

    public function chart_suverv(Request $request)
    {

        $series = array();
        $labels = array();

//        $start = $request->input('start') . ' 00:00:00';
//        $end = $request->input('end') . ' 23:59:00';


        $start = '2022-01-01 00:00:00';
        $end = '2022-12-01' . ' 23:59:00';

        foreach (Suvery::EASY as $index => $status) {
            if ($index != '') {
                $reports = Suvery::whereBetween('created_at', array($start, $end))->where('time', $index)->count();
                array_push($labels, $status);
                array_push($series, $reports);
            }
        }

        $data = array(
            'series' => $series,
            'labels' => $labels
        );

        return response()->json($data, 200);
    }



}

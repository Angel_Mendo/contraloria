<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ReceptionMechanism;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class APIAuth extends Controller
{
    public function userList(Request $request)
    {
        $objects = User::withTrashed();
        return DataTables::of($objects)
            ->addColumn('id', function ($row) {
                return sprintf('%04d', $row->id);;
            })
            ->addColumn('status', function ($row) {
                return $row->get_Status() ? '<span class="badge badge-light-success">Activo</span>' : '<span class="badge badge-light-danger">Inactivo</span>';
            })
            ->addColumn('role', function ($row) {
                return ($row->is_supervisor) ? 'Supervisor' : 'Capturista';
            })
            ->addColumn('unidad', function ($row) {
                return User::ROLES[$row->role];
            })
            ->addColumn('actions', function ($row) {
                $remove = route('util.delete.object', ['model' => 'User', 'pk' => $row->id]);
                if ($row->role == 1 and $row->get_Status()) {
                    $remove = ($row->admins_Count() >= 2) ? route('util.delete.object', ['model' => 'User', 'pk' => $row->id]) : null;
                }

                $actions = array(
                    'url' => route('user.register', ['pk' => $row->id]),
                    'url_remove' => $remove
                );
                return view('snippets.actions')->with('actions', $actions)->with('obj', $row)->render();
            })
//            ->addColumn('actions', function ($row) {
//                return '<div class="d-flex my-2 ms-2"><a href="' . route('user.register', ['pk' => $row->id]) . '" class="btn btn-icon btn-outline-primary w-30px h-30px me-3"><i class="fa fa-edit"></i> </a> </div>';
//            })
            ->rawColumns(['status', 'actions'])
            ->addIndexColumn()
            ->make(true);
    }

}

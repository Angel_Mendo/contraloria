<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ReceptionMechanism;
use App\Models\Report;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class APIReportController extends Controller
{
    public function reportList(Request $request)
    {
        $user_id = $request->input('user_id');
        $start = $request->input('date_start') . ' 00:00:00';
        $end = $request->input('date_end') . ' 23:59:00';

        $objects = Report::all();
        if ($request->input('date_start')) {
            $objects = $objects->whereBetween('reception_date', array($start, $end));
        }

        $user = User::findOrCreate($user_id);

        return DataTables::of($objects)
            ->addColumn('id', function ($row) {
                return sprintf('%04d', $row->id);;
            })
            ->addColumn('report_no', function ($row) {
                return $row->get_ReportNo();
            })
            ->addColumn('mechanism', function ($row) {
                return ReceptionMechanism::findOrCreate($row->mechanism_id)->name;
            })
            ->addColumn('status', function ($row) {
                return $row->get_Status() ? '<span class="badge badge-light-success">Activo</span>' : '<span class="badge badge-light-danger">Inactivo</span>';
            })
            ->addColumn('area', function ($row) {
                return $row->get_Area();
            })

            ->addColumn('actions', function ($row) use ($user) {
                if ($user->role !== 1 and !$user->is_supervisor and $user->role !== $row->get_Area_ID()) {
                    return '';
                }

                $step = $row->get_Area_ID() - 1;

                return '<div class="d-flex my-2 ms-2">
                            <a href="' . route('report.register', ['pk' => $row->get_Folio(), 'step' => $step]) . '" class="btn btn-icon btn-outline-primary w-30px h-30px me-3"><i class="fa fa-edit"></i> </a> 
                            <a href="' . route('denuncia.perfil', ['pk' => $row->get_Folio()]) . '" class="btn btn-icon btn-outline-primary w-30px h-30px me-3"><i class="fa fa-eye"></i> </a> 
                            </div>';
            })
            ->rawColumns(['status', 'actions'])
            ->addIndexColumn()
            ->make(true);
    }
}

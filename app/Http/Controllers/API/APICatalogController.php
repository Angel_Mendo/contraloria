<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Cause;
use App\Models\Dependence;
use App\Models\ReceptionMechanism;
use App\Models\Sanction;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;


class APICatalogController extends Controller
{
    public function mechanismList(Request $request, $builder)
    {
//        if (request()->ajax()) {
        $mechanisms = ReceptionMechanism::withTrashed();
        return DataTables::of($mechanisms)
            ->addColumn('id', function ($row) {
                return sprintf('%04d', $row->id);;
            })
            ->addColumn('status', function ($row) {
                return $row->get_Status() ? '<span class="badge badge-light-success">Activo</span>' : '<span class="badge badge-light-danger">Inactivo</span>';
            })
            ->addColumn('actions', function ($row) {
                $actions = array(
                    'url' => route('catalog.mechanism.list') . '?pk=' . $row->id,
                    'url_remove' => route('util.delete.object', ['model' => 'ReceptionMechanism', 'pk' => $row->id])
                );
                return view('snippets.actions')->with('actions', $actions)->with('obj', $row)->render();
            })
            ->rawColumns(['status', 'actions'])
            ->addIndexColumn()
//            ->setRowId('id')
//            ->toJson();
            ->make(true);
//        }

        $html = $builder->columns([
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'code', 'name' => 'code', 'title' => 'Email'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Created At'],
//            ['data' => 'actions', 'name' => 'updated_at', 'title' => 'Updated At'],
        ]);
        return view('catalog.list', compact('html'));
    }

    public function areaList(Request $request)
    {
        $objects = Area::withTrashed();

        return DataTables::of($objects)
            ->addColumn('id', function ($row) {
                return sprintf('%04d', $row->id);;
            })
            ->addColumn('status', function ($row) {
                return $row->get_Status() ? '<span class="badge badge-light-success">Activo</span>' : '<span class="badge badge-light-danger">Inactivo</span>';
            })
            ->addColumn('actions', function ($row) {
                $actions = array(
                    'url' => route('catalog.area.list') . '?pk=' . $row->id,
                    'url_remove' => route('util.delete.object', ['model' => 'Area', 'pk' => $row->id])
                );
                return view('snippets.actions')->with('actions', $actions)->with('obj', $row)->render();
            })
            ->rawColumns(['status', 'actions'])
            ->addIndexColumn()
            ->make(true);
    }

    public function sanctionList(Request $request)
    {
        $objects = Sanction::withTrashed();
        return DataTables::of($objects)
            ->addColumn('id', function ($row) {
                return sprintf('%04d', $row->id);;
            })
            ->addColumn('status', function ($row) {
                return $row->get_Status() ? '<span class="badge badge-light-success">Activo</span>' : '<span class="badge badge-light-danger">Inactivo</span>';
            })
            ->addColumn('actions', function ($row) {
                $actions = array(
                    'url' => route('catalog.sanction.list') . '?pk=' . $row->id,
                    'url_remove' => route('util.delete.object', ['model' => 'Sanction', 'pk' => $row->id])
                );
                return view('snippets.actions')->with('actions', $actions)->with('obj', $row)->render();
            })
            ->rawColumns(['status', 'actions'])
            ->addIndexColumn()
            ->make(true);
    }

    public function causeList(Request $request)
    {
        $objects = Cause::withTrashed();
        return DataTables::of($objects)
            ->addColumn('id', function ($row) {
                return sprintf('%04d', $row->id);
            })
            ->addColumn('status', function ($row) {
                return $row->get_Status() ? '<span class="badge badge-light-success">Activo</span>' : '<span class="badge badge-light-danger">Inactivo</span>';
            })
            ->addColumn('actions', function ($row) {
                $actions = array(
                    'url' => route('catalog.cause.list') . '?pk=' . $row->id,
                    'url_remove' => route('util.delete.object', ['model' => 'Cause', 'pk' => $row->id])
                );
                return view('snippets.actions')->with('actions', $actions)->with('obj', $row)->render();
            })
            ->rawColumns(['status', 'actions'])
            ->addIndexColumn()
            ->make(true);
    }

    public function dependenceList(Request $request)
    {
        $objects = Dependence::withTrashed();
        return DataTables::of($objects)
            ->addColumn('id', function ($row) {
                return sprintf('%04d', $row->id);
            })
            ->addColumn('status', function ($row) {
                return $row->get_Status() ? '<span class="badge badge-light-success">Activo</span>' : '<span class="badge badge-light-danger">Inactivo</span>';
            })
            ->addColumn('actions', function ($row) {
                $actions = array(
                    'url' => route('catalog.dependence.list') . '?pk=' . $row->id,
                    'url_remove' => route('util.delete.object', ['model' => 'Dependence', 'pk' => $row->id])
                );
                return view('snippets.actions')->with('actions', $actions)->with('obj', $row)->render();
            })
            ->rawColumns(['status', 'actions'])
            ->addIndexColumn()
            ->make(true);
    }
}

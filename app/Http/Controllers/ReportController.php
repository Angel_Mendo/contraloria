<?php

namespace App\Http\Controllers;

use App\Exports\ReportExport;
use App\Imports\ReportImport;
use App\Mail\NewReport;
use App\Models\Dependence;
use App\Models\ReceptionMechanism;
use App\Models\Report;
use App\Models\ReportFile;
use App\Models\ResearchUnit;
use App\Models\SolvingUnit;
use App\Models\SubstanceUnit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use function Psy\debug;

class ReportController extends Controller
{
    public function list(Request $request)
    {
        return view('report.list');
    }

    public function register(Request $request, $pk, $step)
    {
        $folio = $pk;
        $pk = substr($pk, 4);
        $response = array();

        switch ($step) {
            case 1;
                $model = Report::class;
                break;
            case 2;
                $model = ResearchUnit::class;
                break;
            case 3;
                $model = SubstanceUnit::class;
                break;
            case 4;
                $model = SolvingUnit::class;
                break;
        }

        $model = app($model);
        $object = (in_array('report_id', $model->getFillable())) ? $model::getOrNone('report_id', $pk) : $model::findOrCreate($pk);

        if ($request->method() == 'POST') {
            $next = $request->input('next_step');
            $prev = $request->input('prev_step');

//            $model = $request->input('model');

            $data = array(
                'report_id' => $pk
            );

            $object->fill($request->toArray() + $data)->save();
            $folio = (!boolval($pk)) ? $object->get_Folio() : $folio;

            if (!boolval($pk)) {
                return redirect(route('report.register', ['pk' => $folio, 'step' => $step]));
            }

            if ($next) {
                return redirect(route('report.register', ['pk' => $folio, 'step' => intval($step) + 1]));
            }

            if ($prev) {
                return redirect(route('report.register', ['pk' => $folio, 'step' => intval($step) - 1]));
            }

            $response = array(
                'success' => 'Guardado correctamente'
            );
        }

        $files = ReportFile::where('report_id', $pk)
            ->where('unit', $step)->get();

        $report_unit = Report::findOrCreate($pk);

        return view('report.step.step' . $step)
            ->with('files', $files)
            ->with('dependence', ['' => 'Selecciona...'] + Dependence::pluck('name', 'id')->all())
            ->with('message', $response)
            ->with('object', $object)
            ->with('report_unit', $report_unit)
            ->with('folio', $folio)
            ->with('step', $step)
            ->with('steps', Report::STEPS)
            ->with('mechanism', ReceptionMechanism::pluck('name', 'id'))
            ->with('gender', Report::GENDER)
            ->with('type_expediente', Report::TYPE_EXPEDIENTE)
            ->with('gender_d', Report::GENDER_DENUNCIADO)
            ->with('attention_period', range(1, 17))
            ->with('step_selected', $step);
    }

    public function denuncia(Request $request)
    {
        $pk = $request->input('pk') ? $request->input('pk') : 0;
        $object = Report::findOrCreate($pk);
        if ($request->method() == 'POST') {

            $request['accused_charge'] = ($request->get('accused_charge')) ? $request->input('accused_charge') : '-';
            $data = [
                'mechanism_id' => 7,
                'exercise' => date('Y'),
                'update_period' => date('Y-m-d H:i:s')
            ];

            $object->fill($request->toArray() + $data)->save();

            return redirect(route('encuesta.satisfaccion', ['pk' => $object->get_Folio()]));
//            return redirect(route('denuncia.registrada', ['pk' => $object->get_Folio()]));
        }

        $object = Report::findOrCreate(0);
        $gender = Report::GENDER;


        return view('report.denuncia')
            ->with('dependence', ['' => 'Selecciona...'] + Dependence::pluck('name', 'id')->all())
            ->with('gender', $gender)
            ->with('gender_d', Report::GENDER_DENUNCIADO)
            ->with('object', $object);
    }

    public function denuncia_registrada(Request $request, $pk)
    {
        $object = Report::findOrCreate(substr($pk, 4));

//        Mail::to('kanabiiz60@gmail.com')->send(new NewReport($object));

        $response = array(
            'success' => true,
            'message' => 'Denuncia registrada con exito.'
        );

        return view('report.denuncia')
            ->with('response', $response)
            ->with('object', $object);
    }


    public function encuesta_satisfaccion(Request $request, $pk)
    {
        $object = Report::findOrCreate(substr($pk, 4));

//        Mail::to('kanabiiz60@gmail.com')->send(new NewReport($object));

        $response = array(
            'success' => true,
            'message' => 'Denuncia registrada con exito.'
        );

        return view('report.encuesta')
            ->with('response', $response)
            ->with('object', $object);
    }


    public function perfil_denunica(Request $request, $pk)
    {
        $object = Report::findOrCreate(0);

        $folio = $pk;
        if ($folio) {
            $object = Report::findOrCreate(substr($folio, 4));

            $gender = Report::GENDER;
            return view('report.profile')
                ->with('readonly', true)
                ->with('object', $object)
                ->with('gender', $gender)
                ->with('object', $object);
        }
        return view('report.profile')
            ->with('object', $object);

    }

    public function denuncia_seguimiento(Request $request)
    {
        $object = Report::findOrCreate(0);

        $folio = $request->get('folio');

        if ($folio) {
            try {
                $object = Report::findOrCreate(substr($folio, 4));

                if (!$object->id) {
                    $object = Report::where('report_no', 'LIKE', '%' . $folio . '%')->first();
                }
                if ($object->id) {
                    $gender = Report::GENDER;
                    return view('report.denuncia')
                        ->with('readonly', true)
                        ->with('dependence', ['' => 'Selecciona...'] + Dependence::pluck('name', 'id')->all())
                        ->with('object', $object)
                        ->with('gender', $gender)
                        ->with('gender_d', Report::GENDER_DENUNCIADO)
                        ->with('object', $object);
                } else {
                    $response = array(
                        'message' => 'El dato ingresado no corresponde a ningun expediente registrado en el sistema. Favor de verificarlo'
                    );
                }
            } catch (Exception $e) {
                $response = array(
                    'message' => 'El dato ingresado no corresponde a ningun expediente registrado en el sistema. Favor de verificarlo'
                );
            }
        } else {
            $response = array(
                'message' => 'Favor de ingresar el folio que se te asigno.'
            );
        }

        return view('report.seguimiento')
            ->with('message', $response)
            ->with('object', $object);
    }

    public function download(Request $request)
    {
        $start = $request->input('date_start');
        $end = $request->input('date_end');

        return Excel::download(new ReportExport($start, $end), 'report_' . date('Y_m_d') . '.xlsx');
    }

    public function upload(Request $request)
    {
        if (!$request->hasFile('file_to_import')) {
            return response()->json('Selecciona un archivo.', 403);
        }

        $ext = $request->file('file_to_import')->getClientOriginalExtension();
        if (!in_array($ext, ['csv', 'xls', 'xlsx'])) {
            return response()->json('Formato no valido, debes subir uno con extención csv, xls ó xlsx).', 403, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }

        Excel::import(new ReportImport, $request->file('file_to_import'));
        return response()->json([], 200);
    }

    public function file(Request $request)
    {
        $file = $request->file('file');
        $name = $file->getClientOriginalName();
        $ext = trim(explode('.', $name)[1]);
        $name = trim(explode('.', $name)[0]);

        $destino = 'media_files/' . date('Y') . '/' . date('m') . '/';
        $file_path = public_path() . '/' . $destino;

        $media = new ReportFile();
        $media->report_id = Report::findOrCreate(substr($request->input('folio'), 4))->id;
        $media->name = $name;
        $media->path = '/' . $destino . $name . '.' . $ext;
        $media->unit = $request->input('step');
        $media->save();

//        $file->move($destino, $name . '.' . $ext);
        $file->move($file_path, $name . '.' . $ext);
        return response()->json(array('message' => 'Archivo registrado con exito.', 'name' => $name, 'date' => $media->created_at), 200);

    }
}

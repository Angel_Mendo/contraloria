<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class Utils extends Controller
{
    public static function generateSlug($param)
    {
        $param = str_replace(' ', '-', $param);
        $param = strtolower($param);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $param);
    }

    public static function delete_object(Request $request, $model, $pk)
    {
        $model_ = app('App\Models\\' . $model);
        $object = $model_::findOrCreate($pk);
        if ($request->input('forceDelete') == 'true') {
            $object->forceDelete();
        } elseif ($object->deleted_at) {
            $object->restore();
        } else {
            $object->delete();
        }
        return response()->json(['forceDelete' => $request->input('forceDelete')], 200);
    }
}

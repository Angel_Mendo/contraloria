<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    public function __construct()
    {
    }

    public function stats(Request $request)
    {
        $response = array();

        return view('dashboard.stats')->with('message', $response);
    }

    public function dashboard(Request $request)
    {
        $response = array();
        Log::debug(Auth::user()->role);

        $step1 = (Auth::user()->role == 1 or Auth::user()->role == 2) ? 'svg-icon-primary text-primary' : 'svg-icon-disabled text-disabled';
        $step2 = (Auth::user()->role == 1 or Auth::user()->role == 3) ? 'svg-icon-primary text-primary' : 'svg-icon-disabled text-disabled';
        $step3 = (Auth::user()->role == 1 or Auth::user()->role == 4) ? 'svg-icon-primary text-primary' : 'svg-icon-disabled text-disabled';
        $step4 = (Auth::user()->role == 1 or Auth::user()->role == 5) ? 'svg-icon-primary text-primary' : 'svg-icon-disabled text-disabled';

        return view('dashboard.index')
            ->with('step1', $step1)
            ->with('step2', $step2)
            ->with('step3', $step3)
            ->with('step4', $step4)
            ->with('message', $response);
    }
}

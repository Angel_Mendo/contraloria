<?php

namespace App\Exports;

use App\Models\Report;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithProperties;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;


class ReportExport implements FromView, WithColumnFormatting, WithProperties, WithDrawings #FromCollection,
{
    /**
     * @return \Illuminate\Support\Collection
     */
    /*public function collection()
    {
        return Report::all();
    }*/
    public function __construct($start, $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function view(): View
    {
        $start = $this->start . ' 00:00:00';
        $end = $this->end . ' 23:59:00';

        if ($this->start) {
            $objects = Report::whereBetween('reception_date', array($start, $end))->get();
        } else {
            $objects = Report::all();
        }

        return view('report.snippets.export', [
            'objects' => $objects
        ]);
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo Contraloria');
//        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/assets/images/logo-contraloria.png'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('A1');

        $drawing2 = new Drawing();
        $drawing2->setName('Logo GDL');
//        $drawing->setDescription('This is my logo');
        $drawing2->setPath(public_path('/assets/images/logo-gdl.png'));
        $drawing2->setHeight(90);
        $drawing2->setOffsetX(90);
        $drawing2->setCoordinates('BP1');

        return [$drawing, $drawing2];
    }

    public function columnFormats(): array
    {
        return [];
    }

    public function properties(): array
    {
        return [
            'creator' => 'Contraloria en Datos',
            'title' => 'Denuncias',
            'subject' => 'Denuncias',
            'keywords' => 'denuncias,export,spreadsheet',
            'category' => 'Denuncias',
        ];
    }
}

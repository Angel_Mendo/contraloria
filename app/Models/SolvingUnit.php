<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolvingUnit extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'report_solving_unit';

    const ANALYSIS_PRA = [
        1 => 'Estudio',
        2 => 'Diligencias para mejor proveer',
        3 => 'Cierre de instrucción'
    ];

    const RESOLUTION_TYPE = [
        1 => 'Con Sanción',
        2 => 'Sin Sanción',
        3 => 'Con Sanción y Sin Sanción',
        4 => 'Abstención',
        5 => 'seimiento',
    ];

    const SANCTION_TYPE = [
        1 => 'Amonestación pública',
        2 => 'Amonestación privada',
        3 => 'Destitución de su empleo, cargo o comisión',
        4 => 'Inhabilitación temporal ',
        5 => 'Suspensión ',
    ];

    protected $fillable = [
        'report_id',
        'analysis',
        'closing_date',
        'resolution_date',
        'resolution_type',
        'sanction_type',
        'sanction_description',
        'article',
        'sanctioned_person',
        'sanctioned_gender',
        'notification_date',
        'exec_report_date',
        'causing_date',
        'note'
    ];

    public function report_unit()
    {
        return $this->belongsTo(Report::class, 'report_id');
    }

    public function sum_Days()
    {
        if (!$this->resolution_date) {
            return 0;
        }

        $fecha_actual = date("Y/m/d");

        $dias = (strtotime($this->resolution_date) - strtotime($fecha_actual)) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);
        return $dias;
    }

    public function get_Analysis()
    {
        return self::ANALYSIS_PRA[$this->analysis];
    }

    public function get_ResolutionType()
    {
        return self::RESOLUTION_TYPE[$this->resolution_type];
    }

    public function get_sanctionType()
    {
        return self::SANCTION_TYPE[$this->sanction_type];
    }

    public static function findOrCreate($id)
    {
        $obj = static::withTrashed()->find($id);
        return $obj ?: new static;
    }

    public static function getOrNone($field, $data)
    {
        $obj = static::withTrashed()->where($field, $data)->first();
        return $obj ?: new static;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Suvery extends Model
{
    use HasFactory;

    protected $table = 'satisfaction_survey';

    protected $fillable = [
        'easy',
        'time',
        'information',
        'comment'
    ];

    const EASY = [
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
    ];


}

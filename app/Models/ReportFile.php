<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportFile extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'report_file';

    protected $fillable = [
        'report_id',
        'unit',
        'name',
        'path',
        'created_at',
    ];

}

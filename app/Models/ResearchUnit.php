<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResearchUnit extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'report_research_unit';

    const RESEARCH_ORIGIN = [
        1 => 'Actuación de Oficio Contraloría',
        2 => 'Auditorías llevadas  por otros entes fiscalizadores',
        3 => 'Auditorías llevadas  en el OIC Municipal',
        4 => 'Denuncia'

    ];

    const RESEARCH_STATUS = [
        1 => 'Estudio',
        2 => 'Requiriendo información',
        3 => 'Desahogo de diligencias',
        4 => 'Calificación'
    ];

    const QUALIFICATION = [
        1 => 'Calificación como Grave.',
        2 => 'Calificación como No Grave.',
        3 => 'Calificación como Grave y No Grave.',
        4 => 'Acuerdo de conclusión y archivo por falta de elementos.',
        5 => 'No aplica',
    ];

    protected $fillable = [
        'report_id',
        'research_no',
        'reception_date',
        'settlement_date',
        'research_origin',
        'investigated_event',
        'responsible_lawyer',
        'research_status',
        'status_date',
        'qualification',
        'qualification_date',

        'challenge_date',
        'note'
    ];

    public function report_unit()
    {
        return $this->belongsTo(Report::class, 'report_id');
    }

    public function get_ResearchOrigin()
    {
        return self::RESEARCH_ORIGIN[$this->research_origin];
    }

    public function get_ResearchStatus()
    {
        return self::RESEARCH_STATUS[$this->research_status];
    }

    public function sum_Days()
    {
        if (!$this->reception_date) {
            return 0;
        }

        $fecha_actual = date("Y/m/d");

        $dias = (strtotime($this->reception_date) - strtotime($fecha_actual)) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);
        return $dias;
    }

    public static function findOrCreate($id)
    {
        $obj = static::withTrashed()->find($id);
        return $obj ?: new static;
    }

    public static function getOrNone($field, $data)
    {
        $obj = static::withTrashed()->where($field, $data)->first();
        return $obj ?: new static;
    }
}

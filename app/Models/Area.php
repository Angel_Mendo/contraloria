<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'catalog_area';

    protected $fillable = [
        'name',
        'code'
    ];

    public function get_Status()
    {
        return ($this->deleted_at) ? false : true;
    }

    public static function findOrCreate($id)
    {
        $obj = static::withTrashed()->find($id);
        return $obj ?: new static;
    }
}

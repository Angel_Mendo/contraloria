<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubstanceUnit extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'report_substance_unit';

    const TYPE_FOUL = [
        1 => 'Grave',
        2 => 'No grave',
        3 => 'Grave y no grave'
    ];

    const SUBSTANCE_STATUS = [
        1 => 'Admite',
        2 => 'Previene',
        3 => 'Abstención',
        4 => 'Sobreseimiento',
        5 => 'Improcedencia',
        6 => 'Archivo del expediente'

    ];

    protected $fillable = [
        'report_id',
        'reception_date',
        'status',
        'admission_date',
        'process_no',
        'type_foul',
        'presumed_responsible',
        'presumed_responsible_gender',
        'responsible_lawyer',
        'agreement_date',
        'emplacement_date',
        'audience_initial_date',
        'tja_date',
        'tja_no',
        'tja_room',
        'test_date',
        'allegation_date',
        'resolution_area_date',
        'note'
    ];

    public function report_unit()
    {
        return $this->belongsTo(Report::class, 'report_id');
    }

    public function sum_Days()
    {
        if (!$this->reception_date) {
            return 0;
        }

        $fecha_actual = date("Y/m/d");

        $dias = (strtotime($this->reception_date) - strtotime($fecha_actual)) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);
        return $dias;
    }

    public function get_SubstanceStatus()
    {
        return self::SUBSTANCE_STATUS[$this->status];
    }

    public function get_TypeFoul()
    {
        return self::TYPE_FOUL[$this->type_foul];
    }

    public static function findOrCreate($id)
    {
        $obj = static::withTrashed()->find($id);
        return $obj ?: new static;
    }

    public static function getOrNone($field, $data)
    {
        $obj = static::withTrashed()->where($field, $data)->first();
        return $obj ?: new static;
    }
}

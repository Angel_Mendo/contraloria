<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Report extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'report_report';


    const TYPE_EXPEDIENTE = [
        1 => 'De oficio',
        2 => 'Deriavado de una audotoria',
    ];

    const GENDER = [
        1 => 'Masculino',
        2 => 'Femenino',
//        3 => 'Otro',
        4 => 'Prefiero no contestar',
    ];
    const GENDER_DENUNCIADO= [
        1 => 'Masculino',
        2 => 'Femenino',
//        3 => 'Otro',
    ];


    /*const GENDER = [
       1 => 'Hombre',
       2 => 'Mujer',
       3 => 'Ánonimo',
       4 => 'No quiso decir',
       5 => 'No aplica',
       6 => 'Hombre y Mujer',
       7 => 'Hombre y Hombre',
       8 => 'Mujer y Mujer',
       9 => 'Persona jurídica',
   ];*/

    const STEPS = [
        1 => 'UNIDAD DE DENUNCIAS',
        2 => 'UNIDAD DE INVESTIGACIÓN',
        3 => 'UNIDAD DE SUBSTANCIACIÓN',
        4 => 'UNIDAD RESOLUTORA'
    ];

    const INFORMER_TYPE = [
        0 => 'Falta dato',
        1 => 'Dependencia',
        2 => 'Ciudadano',
        3 => 'Órgano Interno de Control',
        4 => 'Auditoría',
        5 => 'Ánonimo',
        6 => 'Persona jurídica',
    ];

    const REPORT_TYPE = [
        1 => 'Administrativa',
        2 => 'Hecho de corrupción',
        3 => 'No competencia',
        4 => 'Queja',
    ];

    const UNIT_PROCESSING = [
        1 => 'Integración',
        2 => 'Turnada a investigación.',
        3 => 'Concluida por incompetencia.',
        4 => 'Concluida por falta de elementos.',
        5 => 'Acumulada a otro expediente.',
        6 => 'Turnada a la Unidad Especializada de Ética e Integridad Pública..'
    ];

    const STATUS = [
        0 => 'Falta dato',
        1 => 'Activa',
        2 => 'Acumulado',
        3 => 'Archivo por Acumulación',
        4 => 'Archivo por falta de elementos',
        5 => 'Archivo por incompetencia',
        6 => 'Derivado',
        7 => 'Estudio',
        8 => 'Turnado e investigación'
    ];

    protected $fillable = [
        'area_id',
        'mechanism_id',
        'exercise',
        'update_period',
        'reception_date',
        'report_no',
        'report_type',
        'informer_type',
        'informer_gender',
        'informer_name',
        'informer_last_name',
        'informer_last_name_2',
        'informer_email',
        'informer_phone',
        'informer_address',
        'accused_name',
        'accused_last_name',
        'accused_last_name_2',
        'accused_dependency',
        'accused_charge',
        'accused_description',
        'reported_event',
        'reported_place',
        'attention_period',
        'unit_processing',
        'status',
        'status_date',
        'commentary',
    ];

    public function get_Status()
    {
        return ($this->deleted_at) ? false : true;
    }

    public function get_ReportNo()
    {
        return $this->report_no;
    }

    public function get_Area()
    {
        $step = 1;
        if (ResearchUnit::getOrNone('report_id', $this->id)->id) {
            $step++;
        }

        if (SubstanceUnit::getOrNone('report_id', $this->id)->id) {
            $step++;
        }

        if (SolvingUnit::getOrNone('report_id', $this->id)->id) {
            $step++;
        }
        return self::STEPS[$step];
    }

    public function get_Area_ID()
    {
        $step = 1;
        if (ResearchUnit::getOrNone('report_id', $this->id)->id) {
            $step++;
        }

        if (SubstanceUnit::getOrNone('report_id', $this->id)->id) {
            $step++;
        }

        if (SolvingUnit::getOrNone('report_id', $this->id)->id) {
            $step++;
        }

        return $step + 1;
    }

    public function get_Folio()
    {
        return $this->exercise . sprintf('%04d', $this->id);
    }

    public function get_ReportType($report_type)
    {
        try {
            return self::REPORT_TYPE[$report_type];
        } catch (\Exception $e) {
            return self::REPORT_TYPE[1];
        }
    }

    public function get_InformerType($informer_type)
    {
        try {
            return self::INFORMER_TYPE[$informer_type];
        } catch (\Exception $e) {
            return self::INFORMER_TYPE[0];
        }

    }

    public function get_Gender($gender)
    {
        try {
            return self::GENDER[$gender];
        } catch (\Exception $e) {
            return self::GENDER[4];
        }
    }

    public function get_UnitProcessing($unit_processing)
    {
        try {
            return self::UNIT_PROCESSING[$unit_processing];
        } catch (\Exception $e) {
            return self::UNIT_PROCESSING[4];
        }
    }

    public function get_ReporStatus($status)
    {
        try {
            return self::STATUS[$status];
        } catch (\Exception $e) {
            return self::STATUS[0];
        }
    }

    public function get_dependency($accused_dependency){
        try{
            return Dependence::find($accused_dependency)->name;
        }catch (\Exception $e){
            return $accused_dependency;
        }
    }
    public function get_Link()
    {
        return 'https://contraloriaendatos.com.mx/denuncia/seguimiento?folio=' . $this->exercise . sprintf('%04d', $this->id);;
    }

    public function mechanism()
    {
        return $this->belongsTo(ReceptionMechanism::class, 'mechanism_id');
    }

    public function research_unit()
    {
        return $this->hasOne(ResearchUnit::class, 'report_id');
    }

    public function substance_unit()
    {
        return $this->hasOne(SubstanceUnit::class, 'report_id');
    }

    public function solving_unit()
    {
        return $this->hasOne(SolvingUnit::class, 'report_id');
    }

    public static function findOrCreate($id)
    {
        $obj = static::withTrashed()->find($id);
        return $obj ?: new static;
    }

    public static function getOrNone($field, $data)
    {
        $obj = static::withTrashed()->where($field, $data)->first();
        return $obj ?: new static;
    }
}

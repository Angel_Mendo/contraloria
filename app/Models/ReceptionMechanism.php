<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceptionMechanism extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'catalog_mechanism';

    protected $fillable = [
        'name',
        'code'
    ];

    public function get_Status()
    {
        return ($this->deleted_at) ? false : true;
    }

    public static function findOrCreate($id)
    {
        $obj = static::withTrashed()->find($id);
        return $obj ?: new static;
    }

    public static function getOrNone($field, $data)
    {
        $obj = static::withTrashed()->where($field, $data)->first();
        return $obj ?: new static;
    }
}

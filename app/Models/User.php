<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $username = 'email';
    
    const ROLES = [
        1 => 'Administrador',
        2 => 'Unidad Denuncias',
        3 => 'Unidad Investigación',
        4 => 'Unidad Substanciación',
        5 => 'Unidad Resolutora',
    ];

    protected $fillable = [
        'role',
        'name',
        'email',
        'is_supervisor',
        'password',
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function findOrCreate($id)
    {
        $obj = static::withTrashed()->find($id);
        return $obj ?: new static;
    }

    public function get_Status()
    {
        return ($this->deleted_at) ? false : true;
    }

    public function admins_Count()
    {
        return static::where('role', 1)->count();
    }
}

@extends('base.backend')

@section('title')
    Areas
@endsection
@section('title')
    Lista
@endsection

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                Registrar
                            </div>
                        </div>
                        <div class="card-body">
                            {{ Form::open(array('method' => 'POST', 'id' =>'main_form', 'enctype' => 'multipart/form-data')) }}
                            {{ Form::hidden('pk', $object->id, array('class' => 'form-control form-control-solid')) }}
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('name', 'Nombre') }}
                                </label>
                                {{ Form::text('name', $object->name, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                            </div>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="fs-5 fw-bold mb-2">
                                    {{ Form::label('code', 'Código') }}
                                </label>
                                {{ Form::text('code', $object->code, array('class' => 'form-control form-control-solid', 'readonly' => 'readonly')) }}
                            </div>

                            <div class="d-flex flex-column mb-5 fv-row">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i> Guardar
                                </button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header border-0 pt-6">
                            <div class="card-title card-title-table">
                                <div class="d-flex align-items-center position-relative my-1">
                            <span class="position-absolute ms-6">
                                <i class="fa fa-search fs-6"></i>
                            </span>
                                    <input type="text" data-kt-table-filter="search"
                                           class="form-control form-control-solid form-control-sm w-250px ps-14"
                                           placeholder="Buscar..."/>
                                </div>
                            </div>
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    <a href="{{ route('catalog.area.list') }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus"></i> Nuevo
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0 table-responsive">
                            <table class="table table-striped align-middle table-row-dashed fs-6 gy-5" id="main-table">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script>
        var data_Columns = [
            {data: 'id', name: 'id', 'title': '#'},
            {data: 'name', name: 'name', 'title': 'Nombre'},
            {data: 'code', name: 'code', 'title': 'Código'},
            {data: 'status', name: 'status', 'title': 'Estatus'},
            {data: 'actions', name: 'actions', 'title': ''},
            // {data: 'action', name: 'action'},
        ];
        $('#main-table').setDataTable('{{ route('api.catalog.area.list') }}', data_Columns);
    </script>
@endsection

@extends('base.backend')

@section('content')
    {!! $html->table() !!}
@endsection

@push('extrajs')
    {!! $html->scripts() !!}
@endpush

<!DOCTYPE html>
<html lang="es">
<!--begin::Head-->
<head>
    <title>Contraloria En Datos - Dash</title>
    <meta name="description" content="Contraloria En Datos"/>
    <meta name="keywords"
          content="Denuncias"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <meta property="og:locale" content="es_MX"/>
    <meta property="og:title"
          content="Contraloria En Datos - Dash"/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom.app.css') }}" rel="stylesheet" type="text/css"/>
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="bg-body">
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Authentication - Sign-in -->
    <div
        class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
        <!--begin::Content-->
        <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
            <!--begin::Logo-->

            <!--end::Logo-->
            <!--begin::Wrapper-->
            <div class="w-lg-500px bg-body p-10 p-lg-15 mx-auto">
                <!--begin::Form-->
                <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form"
                      action="{{ route('admin.login') }}"
                      method="POST">
                {!! csrf_field() !!}
                <!--begin::Heading-->
                    <div class="text-center mb-10">
                        <a href="." class="mb-12">
                            <img alt="Logo" src="{{ asset('assets/images/logo-gdl.png') }}" class="h-200px">
                        </a>
                        <!--begin::Title-->
                        <h1 class="text-dark fw-boldest mb-3 fs-4hx ">DENUNCIA</h1>
                        <!--end::Title-->
                        <!--begin::Link-->
                        <div class="text-gray-400 fw-bold fs-4 bg-primary">
                            Iniciar Sesión
                        </div>
                        <!--end::Link-->
                    </div>
                    <div class="fv-row mb-10 text-center">
                        <label class="form-label fs-6 fw-bolder text-family">
                            Email
                        </label>
                        <input class="form-control form-control-lg form-control-solid" type="text" name="email"
                               autocomplete="off"/>
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="fv-row mb-10 text-center">
                        <!--begin::Wrapper-->

                        <label class="form-label fw-bolder text-family fs-6">
                            Contraseña
                        </label>
                        <!--end::Label-->
                        <input class="form-control form-control-lg form-control-solid" type="password" name="password"
                               autocomplete="off"/>
                        <br>
                        <a href="#" class="fs-6 text-dark fw-bolder mt-4">¿Perdiste tu contraseña?</a>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Actions-->
                    <div class="text-center">
                        <!--begin::Submit button-->
                        <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                            <span class="indicator-label">Continuar</span>
                            <span class="indicator-progress">Espera...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                        <div class="fv-plugins-message-container invalid-feedback">
                            <div data-field="password" data-validator="notEmpty">
                                @if(isset($message))
                                    {{ $message['message']  }}
                                @endif

                                @if(isset($_GET['message']))
                                    {{$_GET['message']}}
                                @endif

                            </div>
                        </div>
                        <!--end::Submit button-->
                        <!--begin::Separator-->
                    </div>
                    <!--end::Actions-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Content-->
        <!--begin::Footer-->
        <div class="d-flex flex-center flex-column-auto p-10">
            <!--begin::Links-->
            <div class="d-flex align-items-center fw-bold fs-6">

            </div>
            <!--end::Links-->
        </div>
        <!--end::Footer-->
    </div>
    <!--end::Authentication - Sign-in-->
</div>
<!--end::Main-->
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{% static 'assets/js/plugins.bundle.js' %}"></script>
<script src="{% static 'assets/js/scripts.bundle.js' %}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{% static 'assets/js/auth/general.js' %}"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
<style>
    .card {
        border: none;
    }
</style>
</body>
<!--end::Body-->
</html>

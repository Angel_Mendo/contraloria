@extends('base.backend')

@section('title')
    Usuarios
@endsection

@section('subtitle')
    Registro
@endsection

@php($role = \App\Models\User::ROLES)

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-3">

                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                Registrar
                            </div>
                        </div>
                        <div class="card-body">
                            {{ Form::open(array('method' => 'POST', 'id' =>'main_form', 'enctype' => 'multipart/form-data')) }}
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('name', 'Nombre') }}
                                </label>
                                {{ Form::text('name', $object->name, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                            </div>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('email', 'Correo') }}
                                </label>
                                {{ Form::text('email', $object->email, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                            </div>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('role', 'Unidad') }}
                                </label>
                                {{ Form::select('role', $role, $object->role, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                            </div>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('is_supervisor', 'Rol') }}
                                </label>
                                {{ Form::select('is_supervisor',  [0 => 'Capturista', 1 => 'Supervisor'], $object->is_supervisor, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                            </div>

                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="fs-5 fw-bold mb-2">
                                    {{ Form::label('password', 'Contraseña') }}
                                </label>
                                {{ Form::password('password', array('class' => 'form-control form-control-solid')) }}
                            </div>

                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="fs-5 fw-bold mb-2">
                                    {{ Form::label('password2', 'Repite Contraseña') }}
                                </label>
                                {{ Form::password('password2', array('class' => 'form-control form-control-solid')) }}
                            </div>

                            <div class="d-flex flex-column mb-5 fv-row text-center">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i> Guardar
                                </button>
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="password" data-validator="notEmpty">
                                        @if(isset($response))
                                            {{ $response['message']  }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script>
        var data_Columns = [
            {data: 'id', name: 'id', 'title': '#'},
            {data: 'name', name: 'name', 'title': 'Usuario'},
            {data: 'email', name: 'email', 'title': 'Correo'},
            {data: 'role', name: 'role', 'title': 'Rol'},
            {data: 'created_at', name: 'created_at', 'title': 'Fecha de registro'},
            {data: 'status', name: 'status', 'title': 'Estatus'},
            {data: 'actions', name: 'actions', 'title': ''},
            // {data: 'action', name: 'action'},
        ];
        $('#main-table').setDataTable('{{ route('api.user.list') }}', data_Columns);
        {{--$('#main-table').setDataTable('{{ route('api.report.list') }}');--}}
    </script>
@endsection


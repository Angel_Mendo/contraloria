@extends('base.backend')

@section('title')
    Usuarios
@endsection

@section('subtitle')
    Lista
@endsection


@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">

            <div class="card">
                <div class="card-header border-0 pt-6">
                    <div class="card-title card-title-table">
                        <div class="d-flex align-items-center position-relative my-1">
                            <span class="position-absolute ms-6">
                                <i class="fa fa-search fs-6"></i>
                            </span>
                            <input type="text" data-kt-table-filter="search"
                                   class="form-control form-control-solid form-control-sm w-250px ps-14"
                                   placeholder="Buscar..."/>
                        </div>
                    </div>
                    <div class="card-toolbar">
                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                            <a href="{{ route('user.register', ['pk' => 00000]) }}" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Nuevo
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0 table-responsive">

                    <table class="table table-striped align-middle table-row-dashed fs-6 gy-5" id="main-table">

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script>
        var data_Columns = [
            {data: 'id', name: 'id', 'title': '#'},
            {data: 'name', name: 'name', 'title': 'Usuario'},
            {data: 'email', name: 'email', 'title': 'Correo'},
            {data: 'unidad', name: 'unidad', 'title': 'Unidad'},
            {data: 'role', name: 'role', 'title': 'Rol'},
            {data: 'created_at', name: 'created_at', 'title': 'Fecha de registro'},
            {data: 'status', name: 'status', 'title': 'Estatus'},
            {data: 'actions', name: 'actions', 'title': ''},
            // {data: 'action', name: 'action'},
        ];
        $('#main-table').setDataTable('{{ route('api.user.list') }}', data_Columns);
        {{--$('#main-table').setDataTable('{{ route('api.report.list') }}');--}}
    </script>
@endsection


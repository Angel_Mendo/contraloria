<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
<head>
    <base href="">
    <title>Contraloria en Datos - Home</title>
    <meta name="CSRF-Token" content="{{ csrf_token() }}"/>
    <meta name="description" content="Contraloria en datos"/>
    <meta name="keywords"
          content="Contraloria, Datos"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <meta property="og:locale" content="es_MX"/>
    <meta property="og:title"
          content="BB - Dash"/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <link rel="shortcut icon" href="{{ asset('assets/images/logo-gdl.png') }}"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>

    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom.app.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/fonts/all.min.css') }}" media="screen" rel="stylesheet"
          type="text/css"/>

</head>
<body id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" data-bs-offset="200"
      class="bg-white position-relative">
<div class="d-flex flex-column flex-root">
    <div class="mb-0" id="home">
        <div class="bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom landing-dark-bg">
            <div class="landing-header" data-kt-sticky="true" data-kt-sticky-name="landing-header"
                 data-kt-sticky-offset="{default: '200px', lg: '300px'}">
                <div class="container">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center flex-equal">
                            <button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none"
                                    id="kt_landing_menu_toggle">
                                <span class="svg-icon svg-icon-2hx">
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                         viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <rect fill="#000000" x="4" y="5" width="16" height="3" rx="1.5"/>
                                            <path
                                                d="M5.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 Z M5.5,10 L18.5,10 C19.3284271,10 20,10.6715729 20,11.5 C20,12.3284271 19.3284271,13 18.5,13 L5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z"
                                                fill="#000000" opacity="0.3"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                            <a href="#">
                                <img alt="Logo" src="{{ asset('assets/images/logo_contraloria_blanco.svg') }}"
                                     class="logo-default h-60px h-lg-60px"/>
                                <img alt="Logo" src="{{ asset('assets/images/logo-gdl.png') }}"
                                     class="logo-sticky h-20px h-lg-25px"/>
                            </a>
                        </div>
                        <div class="d-lg-block" id="kt_header_nav_wrapper">
                            <div class="d-lg-block p-5 p-lg-0" data-kt-drawer="true" data-kt-drawer-name="landing-menu"
                                 data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                                 data-kt-drawer-width="200px" data-kt-drawer-direction="start"
                                 data-kt-drawer-toggle="#kt_landing_menu_toggle" data-kt-swapper="true"
                                 data-kt-swapper-mode="prepend"
                                 data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav_wrapper'}">
                                <!--begin::Menu-->
                                <div
                                    class="menu menu-column flex-nowrap menu-rounded menu-lg-row menu-title-gray-500 menu-state-title-primary nav nav-flush fs-5 fw-bold"
                                    id="kt_landing_menu">
                                    <!--begin::Menu item-->
                                    <div class="menu-item">
                                        <!--begin::Menu link-->
                                        <a class="menu-link nav-link  py-3 px-4 px-xxl-6"
                                           href="{{ route('landing') }}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                            Home
                                        </a>
                                    </div>
                                    <div class="menu-item">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" target="_blank"
                                           href="{{ route('denuncia.registro') }}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                            Registra tu Denuncia
                                        </a>
                                    </div>
                                    <div class="menu-item">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" target="_blank"
                                           href="{{ route('denuncia.seguimiento') }}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                            Seguimiento de Denuncia
                                        </a>
                                    </div>
                                    <div class="menu-item">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" href="{{route('estadisticas')}}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Estadísticas</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <div class="landing-curve landing-dark-color mb-10 mb-lg-20">
            <svg viewBox="15 12 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M0 11C3.93573 11.3356 7.85984 11.6689 11.7725 12H1488.16C1492.1 11.6689 1496.04 11.3356 1500 11V12H1488.16C913.668 60.3476 586.282 60.6117 11.7725 12H0V11Z"
                    fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <div class="mb-n10 mb-lg-n20 z-index-2">
        <div class="container">
            <div class="row w-100 gy-10 mb-md-20">
                <div class="col-md-12 ">
                <h1 class="text-center" style="font-size: 22px; line-height: 1.5">Aviso de Privacidad Corto Focalizado a la página de internet Contraloría en Datos </h1>
                <p class="text-justify pt-4" style="font-size: 18px;">El Gobierno Municipal de Guadalajara, a través de la Contraloría Ciudadana de
                    Guadalajara, con domicilio en la Avenida 5 de febrero #249, Unidad Administrativa Reforma, en la
                    colonia Las Conchas, C.P. 44460, de la ciudad de Guadalajara es el responsable de la protección de
                    sus datos personales que nos proporcione y al respecto le informa lo siguiente:<br><br>
                    Sus datos personales se recaban única y exclusivamente para llevar a cabo los objetivos y
                    atribuciones de la Contraloría Ciudadana de Guadalajara en los términos que establece la normatividad
                    aplicable, siendo las siguientes:</p>

                    <ul style="font-size: 18px;">
                        <li>Recibir y tramitar denuncias a servidores públicos del Gobierno Municipal de Guadalajara
                            por faltas administrativas y/o actos de corrupción.</li>
                        <li>Derivar las denuncias que no sean competencia de la Contraloría Ciudadana de Guadalajara.</li>
                        <li>Llevar un control de expedientes tramitados en la dependencia.</li>
                        <li>Generar estadísticas que serán publicadas en la página
                            <a href="www.contraloriaendatos.com.mx"> www.contraloriaendatos.com.mx </a></li>
                    </ul>

                    <p style="font-size: 18px;"> Si quiere conocer el Aviso de Privacidad Integral puede consultarlo en la siguiente liga:
                     <a href="https://guadalajara.gob.mx/aviso-privacidad.pdf" target="_blank"> https://guadalajara.gob.mx/aviso-privacidad.pdf
                     </a>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="mb-0 pt-4">
        <!--begin::Curve top-->
        <div class="landing-curve landing-dark-color">
            <svg viewBox="15 -1 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M1 48C4.93573 47.6644 8.85984 47.3311 12.7725 47H1489.16C1493.1 47.3311 1497.04 47.6644 1501 48V47H1489.16C914.668 -1.34764 587.282 -1.61174 12.7725 47H1V48Z"
                    fill="currentColor"></path>
            </svg>
        </div>
        <div class="landing-dark-bg pt-20 pb-10">
            <div class="container">
                <div class="row text-white">
                    <div class="col-md-4 text-center">
                        <h5 class="text-white">Datos de Contacto</h5>
                        <p>
                            Telefono: 36691300 ext. 8238
                            <br>
                            informescontraloria@guadalajara.gob.mx
                            <br>
                            Av. 5 de Febrero 249, U.A. Reforma, Col. Las Conchas, C.P. 44460.

                        </p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img alt="Logo" src="{{ asset('assets/images/logo-gdl.png') }}" width="100px">
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="https://www.facebook.com/GobiernoJalisco" target="_blank">
                            <img width="50px" class="m-5" src="{{ asset('assets/images/facebook.png') }}">
                        </a>
                        <a href="https://twitter.com/GobiernoJalisco" target="_blank">
                            <img width="50px" class="m-5" src="{{ asset('assets/images/twitter.png') }}">
                        </a>
                        <a href="https://www.instagram.com/gobjalisco/" target="_blank">
                            <img width="50px" class="m-5" src="{{ asset('assets/images/instagram.png') }}">
                        </a>
                        <a href="https://www.youtube.com/channel/UCQBDDUradzK_8kVbLHvzVeg" target="_blank">
                            <img width="50px" class="m-5" src="{{ asset('assets/images/youtube.svg') }}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="{{ asset('assets/js/plugins.bundle.js') }}"></script>
<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>

<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/chart/charts.js') }}"></script>
<script src="{{ asset('assets/fonts/all.min.js') }}"></script>
<script>
    var chart_informer_gender = ChartFunction.initChartPie('#chart_informer_gender', '{{ route('api.chart.informer_gender') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'));
    var chart_report = ChartFunction.initChart('#chart_report', '{{ route('api.chart.chart_report') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'));
</script>

</body>
</html>

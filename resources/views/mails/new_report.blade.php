<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nueva Denuncia</title>
</head>
<body>
<p>Se ha registrado una nueva denuncia <b>#{{ $object->get_Folio() }}</b> a las {{ $object->created_at }}.</p>
<p>Estos son los datos del usuario que ha realizado la denuncia:</p>
<ul>
    <li>Nombre: {{ $object->informer_name }} {{ $object->informer_last_name }} </li>
    <li>Teléfono: {{ $object->phone }} </li>
    <li>Email: {{ $object->email }}</li>
</ul>
</body>
</html>

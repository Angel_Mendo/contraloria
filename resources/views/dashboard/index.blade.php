@extends('base.backend')

@section('title')
    Dashboard
@endsection

@section('subtitle')
    Dashboard
@endsection

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="row">
                    <div class="col-md-3 mb-4 pb-2">
                        <div class="bg-light-dark px-6 py-6 rounded-2 text-center">
                            <a href="{{url('admin/report/list')}}"
                               class="{{ $step1 }} fw-bold fs-3">
                                <img alt="" src="{{ asset('assets/images/unidad_de_denuncias.svg') }}" height="150px">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-2">
                        <div class="bg-light-dark px-4 py-6 rounded-2 text-center">
                            <a href="{{url('admin/report/list')}}"
                               class="{{ $step2 }} fw-bold fs-3">
                                <img alt="" src="{{ asset('assets/images/unidad_de_investigacion.svg') }}" height="150px">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-4">
                        <div class="bg-light-dark px-4 py-6 rounded-2 text-center">
                            <a href="{{url('admin/report/list')}}"
                               class="{{ $step3 }} fw-bold fs-3">
                                <img alt="" src="{{ asset('assets/images/unidad_de_subtratacion.svg') }}" height="150px">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-4">
                        <div class="bg-light-dark px-4 py-6 rounded-2 text-center">
                            <a href="{{url('admin/report/list')}}"
                               class="{{ $step4 }} fw-bold fs-3">
                                <img alt="" src="{{ asset('assets/images/unidad_resolucion.svg') }}" height="150px">

                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .text-primary,
        .svg-icon-primary svg,
        .svg-icon.svg-icon-primary svg [fill]:not(.permanent):not(g) {
            color: #FC637D !important;
            fill: #FC637D !important;
        }

        .text-disabled {
            color: #a1a5b7!important;
        }
    </style>
@endsection
@section('extrajs')

@endsection

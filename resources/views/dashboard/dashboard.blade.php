@extends('base.backend')

@section('title')
    Dashboard
@endsection

@section('subtitle')
    Dashboard
@endsection

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="col-md-12 mb-5">
                    <div class="card">
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">
                                    Denuncias
                                </span>
                            </h3>
                            <div class="card-toolbar">
                                <div class="form-group mb-5">
                                    <label class="fs-5 fw-bold mb-2">
                                        Fecha:
                                    </label>
                                    <input type="text" name="range_chart_report"
                                           class="form-control form-control-sm form-control-solid date_range">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="chart_report"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-5">
                    <div class="card">
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">
                                    Denuncias Por Sexo
                                </span>
                            </h3>
                            <div class="card-toolbar">
                                <div class="form-group mb-5">
                                    <label class="fs-5 fw-bold mb-2">
                                        Fecha:
                                    </label>
                                    <input type="text" name="range_chart_informer_gender"
                                           class="form-control form-control-sm form-control-solid date_range">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="chart_informer_gender"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-5">
                    <div class="card">
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">
                                    Tipo de Denunciante
                                </span>
                            </h3>
                            <div class="card-toolbar">
                                <div class="form-group mb-5">
                                    <label class="fs-5 fw-bold mb-2">
                                        Fecha:
                                    </label>
                                    <input type="text" name="range_chart_informer_type"
                                           class="form-control form-control-sm form-control-solid date_range">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="chart_informer_type"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('extrajs')
@endsection

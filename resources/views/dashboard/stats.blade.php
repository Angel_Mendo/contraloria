@extends('base.backend')

@section('title')
    Dashboard
@endsection

@section('subtitle')
    Dashboard
@endsection

@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="row">
                <div class="col-md-12 mb-5">

                    <div class="card">
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">
                                    Promedio de días de atención por unidad
                                </span>
                            </h3>
                        </div>
                        <div class="row">
                        <div class="col-md-3">
                            <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain">

                            <span class="svg-icon svg-icon-2tx svg-icon-dark mb-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.25"
                                          d="M3.19406 11.1644C3.09247 10.5549 3.56251 10 4.18045 10H19.8195C20.4375 10 20.9075 10.5549 20.8059 11.1644L19.4178 19.4932C19.1767 20.9398 17.9251 22 16.4586 22H7.54138C6.07486 22 4.82329 20.9398 4.58219 19.4932L3.19406 11.1644Z"
                                          fill="#7E8299"/>
                                    <path
                                            d="M2 9.5C2 8.67157 2.67157 8 3.5 8H20.5C21.3284 8 22 8.67157 22 9.5C22 10.3284 21.3284 11 20.5 11H3.5C2.67157 11 2 10.3284 2 9.5Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M10 13C9.44772 13 9 13.4477 9 14V18C9 18.5523 9.44772 19 10 19C10.5523 19 11 18.5523 11 18V14C11 13.4477 10.5523 13 10 13Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M14 13C13.4477 13 13 13.4477 13 14V18C13 18.5523 13.4477 19 14 19C14.5523 19 15 18.5523 15 18V14C15 13.4477 14.5523 13 14 13Z"
                                            fill="#7E8299"/>
                                    <g opacity="0.25">
                                        <path
                                                d="M10.7071 3.70711C11.0976 3.31658 11.0976 2.68342 10.7071 2.29289C10.3166 1.90237 9.68342 1.90237 9.29289 2.29289L4.29289 7.29289C3.90237 7.68342 3.90237 8.31658 4.29289 8.70711C4.68342 9.09763 5.31658 9.09763 5.70711 8.70711L10.7071 3.70711Z"
                                                fill="#7E8299"/>
                                        <path
                                                d="M13.2929 3.70711C12.9024 3.31658 12.9024 2.68342 13.2929 2.29289C13.6834 1.90237 14.3166 1.90237 14.7071 2.29289L19.7071 7.29289C20.0976 7.68342 20.0976 8.31658 19.7071 8.70711C19.3166 9.09763 18.6834 9.09763 18.2929 8.70711L13.2929 3.70711Z"
                                                fill="#7E8299"/>
                                    </g>
                                </svg>
                            </span>
                                <div class="mb-0">
                                    <div class="fs-lg-2hx fs-2x fw-bolder text-dark d-flex flex-center text-center">
                                        <div class="min-w-70px" data-kt-countup="true" data-kt-countup-value="4"
                                             data-kt-countup-suffix="">4
                                        </div>
                                    </div>
                                    <span class="text-dark-600 fw-bold fs-5 lh-0">UNIDAD DE DENUNCIA</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain">

                            <span class="svg-icon svg-icon-2tx svg-icon-dark mb-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.25"
                                          d="M3.19406 11.1644C3.09247 10.5549 3.56251 10 4.18045 10H19.8195C20.4375 10 20.9075 10.5549 20.8059 11.1644L19.4178 19.4932C19.1767 20.9398 17.9251 22 16.4586 22H7.54138C6.07486 22 4.82329 20.9398 4.58219 19.4932L3.19406 11.1644Z"
                                          fill="#7E8299"/>
                                    <path
                                            d="M2 9.5C2 8.67157 2.67157 8 3.5 8H20.5C21.3284 8 22 8.67157 22 9.5C22 10.3284 21.3284 11 20.5 11H3.5C2.67157 11 2 10.3284 2 9.5Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M10 13C9.44772 13 9 13.4477 9 14V18C9 18.5523 9.44772 19 10 19C10.5523 19 11 18.5523 11 18V14C11 13.4477 10.5523 13 10 13Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M14 13C13.4477 13 13 13.4477 13 14V18C13 18.5523 13.4477 19 14 19C14.5523 19 15 18.5523 15 18V14C15 13.4477 14.5523 13 14 13Z"
                                            fill="#7E8299"/>
                                    <g opacity="0.25">
                                        <path
                                                d="M10.7071 3.70711C11.0976 3.31658 11.0976 2.68342 10.7071 2.29289C10.3166 1.90237 9.68342 1.90237 9.29289 2.29289L4.29289 7.29289C3.90237 7.68342 3.90237 8.31658 4.29289 8.70711C4.68342 9.09763 5.31658 9.09763 5.70711 8.70711L10.7071 3.70711Z"
                                                fill="#7E8299"/>
                                        <path
                                                d="M13.2929 3.70711C12.9024 3.31658 12.9024 2.68342 13.2929 2.29289C13.6834 1.90237 14.3166 1.90237 14.7071 2.29289L19.7071 7.29289C20.0976 7.68342 20.0976 8.31658 19.7071 8.70711C19.3166 9.09763 18.6834 9.09763 18.2929 8.70711L13.2929 3.70711Z"
                                                fill="#7E8299"/>
                                    </g>
                                </svg>
                            </span>
                                <div class="mb-0">
                                    <div class="fs-lg-2hx fs-2x fw-bolder text-dark d-flex flex-center text-center">
                                        <div class="min-w-70px" data-kt-countup="true" data-kt-countup-value="7"
                                             data-kt-countup-suffix="">7
                                        </div>
                                    </div>
                                    <span class="text-dark-600 fw-bold fs-5 lh-0">UNIDAD DE INVESTIGACIÓN</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain">

                            <span class="svg-icon svg-icon-2tx svg-icon-dark mb-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.25"
                                          d="M3.19406 11.1644C3.09247 10.5549 3.56251 10 4.18045 10H19.8195C20.4375 10 20.9075 10.5549 20.8059 11.1644L19.4178 19.4932C19.1767 20.9398 17.9251 22 16.4586 22H7.54138C6.07486 22 4.82329 20.9398 4.58219 19.4932L3.19406 11.1644Z"
                                          fill="#7E8299"/>
                                    <path
                                            d="M2 9.5C2 8.67157 2.67157 8 3.5 8H20.5C21.3284 8 22 8.67157 22 9.5C22 10.3284 21.3284 11 20.5 11H3.5C2.67157 11 2 10.3284 2 9.5Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M10 13C9.44772 13 9 13.4477 9 14V18C9 18.5523 9.44772 19 10 19C10.5523 19 11 18.5523 11 18V14C11 13.4477 10.5523 13 10 13Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M14 13C13.4477 13 13 13.4477 13 14V18C13 18.5523 13.4477 19 14 19C14.5523 19 15 18.5523 15 18V14C15 13.4477 14.5523 13 14 13Z"
                                            fill="#7E8299"/>
                                    <g opacity="0.25">
                                        <path
                                                d="M10.7071 3.70711C11.0976 3.31658 11.0976 2.68342 10.7071 2.29289C10.3166 1.90237 9.68342 1.90237 9.29289 2.29289L4.29289 7.29289C3.90237 7.68342 3.90237 8.31658 4.29289 8.70711C4.68342 9.09763 5.31658 9.09763 5.70711 8.70711L10.7071 3.70711Z"
                                                fill="#7E8299"/>
                                        <path
                                                d="M13.2929 3.70711C12.9024 3.31658 12.9024 2.68342 13.2929 2.29289C13.6834 1.90237 14.3166 1.90237 14.7071 2.29289L19.7071 7.29289C20.0976 7.68342 20.0976 8.31658 19.7071 8.70711C19.3166 9.09763 18.6834 9.09763 18.2929 8.70711L13.2929 3.70711Z"
                                                fill="#7E8299"/>
                                    </g>
                                </svg>
                            </span>
                                <div class="mb-0">
                                    <div class="fs-lg-2hx fs-2x fw-bolder text-dark d-flex flex-center text-center">
                                        <div class="min-w-70px" data-kt-countup="true" data-kt-countup-value="12"
                                             data-kt-countup-suffix="">12
                                        </div>
                                    </div>
                                    <span class="text-dark-600 fw-bold fs-5 lh-0">UNIDAD DE SUBSTANCIACIÓN</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain">

                            <span class="svg-icon svg-icon-2tx svg-icon-dark mb-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.25"
                                          d="M3.19406 11.1644C3.09247 10.5549 3.56251 10 4.18045 10H19.8195C20.4375 10 20.9075 10.5549 20.8059 11.1644L19.4178 19.4932C19.1767 20.9398 17.9251 22 16.4586 22H7.54138C6.07486 22 4.82329 20.9398 4.58219 19.4932L3.19406 11.1644Z"
                                          fill="#7E8299"/>
                                    <path
                                            d="M2 9.5C2 8.67157 2.67157 8 3.5 8H20.5C21.3284 8 22 8.67157 22 9.5C22 10.3284 21.3284 11 20.5 11H3.5C2.67157 11 2 10.3284 2 9.5Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M10 13C9.44772 13 9 13.4477 9 14V18C9 18.5523 9.44772 19 10 19C10.5523 19 11 18.5523 11 18V14C11 13.4477 10.5523 13 10 13Z"
                                            fill="#7E8299"/>
                                    <path
                                            d="M14 13C13.4477 13 13 13.4477 13 14V18C13 18.5523 13.4477 19 14 19C14.5523 19 15 18.5523 15 18V14C15 13.4477 14.5523 13 14 13Z"
                                            fill="#7E8299"/>
                                    <g opacity="0.25">
                                        <path
                                                d="M10.7071 3.70711C11.0976 3.31658 11.0976 2.68342 10.7071 2.29289C10.3166 1.90237 9.68342 1.90237 9.29289 2.29289L4.29289 7.29289C3.90237 7.68342 3.90237 8.31658 4.29289 8.70711C4.68342 9.09763 5.31658 9.09763 5.70711 8.70711L10.7071 3.70711Z"
                                                fill="#7E8299"/>
                                        <path
                                                d="M13.2929 3.70711C12.9024 3.31658 12.9024 2.68342 13.2929 2.29289C13.6834 1.90237 14.3166 1.90237 14.7071 2.29289L19.7071 7.29289C20.0976 7.68342 20.0976 8.31658 19.7071 8.70711C19.3166 9.09763 18.6834 9.09763 18.2929 8.70711L13.2929 3.70711Z"
                                                fill="#7E8299"/>
                                    </g>
                                </svg>
                            </span>
                                <div class="mb-0">
                                    <div class="fs-lg-2hx fs-2x fw-bolder text-dark d-flex flex-center text-center">
                                        <div class="min-w-70px" data-kt-countup="true" data-kt-countup-value="0"
                                             data-kt-countup-suffix="">0
                                        </div>
                                    </div>
                                    <span class="text-dark-600 fw-bold fs-5 lh-0">UNIDAD RESOLUTORA</span>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mb-5">
                    <div class="card">
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">
                                    Denuncias Por Sexo
                                </span>
                            </h3>
                            <div class="card-toolbar">
                                <div class="form-group mb-5">
                                    <label class="fs-5 fw-bold mb-2">
                                        Fecha:
                                    </label>
                                    <input type="text" name="range_chart_informer_gender"
                                           class="form-control form-control-sm form-control-solid date_range">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="chart_informer_gender"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-5">
                    <div class="card">
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">
                                    Tipo de Denunciante
                                </span>
                            </h3>
                            <div class="card-toolbar">
                                <div class="form-group mb-5">
                                    <label class="fs-5 fw-bold mb-2">
                                        Fecha:
                                    </label>
                                    <input type="text" name="range_chart_informer_type"
                                           class="form-control form-control-sm form-control-solid date_range">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="chart_informer_type"></div>
                        </div>

                </div>

            </div>
                <div class="col-md-6 mb-5">
                    <div class="card">
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">
                                    ¿Qué tan fácil fue hacer tu denuncia?
                                </span>
                            </h3>
                            <div class="card-toolbar">
                                <div class="form-group mb-5">
                                    <label class="fs-5 fw-bold mb-2">
                                        Fecha:
                                    </label>
                                    <input type="text" name="range_chart_informer_suvery"
                                           class="form-control form-control-sm form-control-solid date_range">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="chart_informer_suvery"></div>
                        </div>

                </div>

            </div>

            </div>
    </div>
@endsection
@section('extrajs')
    <script src="{{ asset('assets/js/chart/charts.js') }}"></script>
    <script>
        var chart_report = ChartFunction.initChartPie('#chart_report', '{{ route('api.chart.chart_report') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'));
        var chart_informer_gender = ChartFunction.initChartPie('#chart_informer_gender', '{{ route('api.chart.informer_gender') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'));
        var chart_informer_type = ChartFunction.initChartPie('#chart_informer_type', '{{ route('api.chart.informer_type') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'));
        var chart_informer_suvery = ChartFunction.initChartPie('#chart_informer_suvery', '{{ route('api.chart.suvery') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'));

        $('input[name="range_chart_report"]').on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
            ChartFunction.updateChart(chart_report, '{{ route('api.chart.chart_report') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'), chart_informer_gender)
        });

        $('input[name="range_chart_informer_gender"]').on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
            ChartFunction.updateChartPie(chart_informer_gender, '{{ route('api.chart.informer_gender') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'), chart_informer_gender)
        });

        $('input[name="range_chart_informer_type"]').on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
            ChartFunction.updateChartPie(chart_informer_type, '{{ route('api.chart.informer_type') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'), chart_informer_gender)
        });


        $('input[name="range_chart_informer_suvery"]').on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
            ChartFunction.updateChartPie(chart_informer_suvery, '{{ route('api.chart.suvery') }}?start=' + start.format('YYYY-MM-DD') + '&end=' + end.format('YYYY-MM-DD'), chart_informer_gender)
        });
    </script>
@endsection

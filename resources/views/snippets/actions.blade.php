<div class="d-flex my-2 ms-2">
    <a href="{{ $actions['url'] }}" class="btn btn-icon btn-outline-primary w-30px h-30px me-3">
        <i class="fa fa-edit"></i>
    </a>

    @if(isset($actions['url_remove']))
        @if(!$obj['deleted_at'])
            <a href="#" class="btn btn-icon btn-outline-danger w-30px h-30px me-3 data-remove" data-bs-toggle="tooltip"
               title="Eliminar"
               data-url="{{ $actions['url_remove'] }}">
                <i class="fa fa-times"></i>
            </a>
        @else
            <a href="#" class="btn btn-icon btn-outline-primary w-30px h-30px me-3 data-active" data-bs-toggle="tooltip"
               title="Activar"
               data-url="{{ $actions['url_remove'] }}">
                <i class="fa fa-check"></i>
            </a>
        @endif
    @endif
</div>

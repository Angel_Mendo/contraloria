<!DOCTYPE html>
<html lang="es">
<!--begin::Head-->
<head>
    <title>Contraloria En Datos - Denuncia</title>
    <meta name="description" content="Contraloria En Datos"/>
    <meta name="keywords"
          content="Denuncias"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <meta property="og:locale" content="es_MX"/>
    <meta property="og:title"
          content="Contraloria En Datos - Denuncia"/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
{{--    <link rel="shortcut icon" href="{{ asset('assets/images/logo-gdl.png') }}"/>--}}
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
{{--    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/css/custom.app.css') }}" rel="stylesheet" type="text/css"/>
    <!--end::Global Stylesheets Bundle-->
    <style>
        .card {
            border: none;
        }

        .card-header {
            width: 100% !important;
            border-top-left-radius: 70px;
            border-top-right-radius: 70px;
            padding: 1rem !important;
            display: initial !important;
        }
        form label{
            color:gray;
        }

        label {
            font-weight: bold !important;
            text-transform: uppercase !important;
            font-size: 15px !important;
        }

        .folio {
            font-size: 30px !important;
        }

        .hide {
            display: none;
        }
        .form-check.form-check-solid .form-check-input {
            border: 0;
            background-color: #4f458f !important;
        }
        .btn_morado{
            background-color: #4f458f !important;
        }
    </style>
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="bg-body">
<div class="mb-0" id="home">
    <div class="bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom landing-dark-bg">
        <div class="landing-header" style="height: 70px !important;" data-kt-sticky="true" data-kt-sticky-name="landing-header"
             data-kt-sticky-offset="{default: '200px', lg: '300px'}">
            <div class="container">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center flex-equal">
                        <button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none"
                                id="kt_landing_menu_toggle">
                                <span class="svg-icon svg-icon-2hx">
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                         viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <rect fill="#000000" x="4" y="5" width="16" height="3" rx="1.5"/>
                                            <path
                                                    d="M5.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 Z M5.5,10 L18.5,10 C19.3284271,10 20,10.6715729 20,11.5 C20,12.3284271 19.3284271,13 18.5,13 L5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z"
                                                    fill="#000000" opacity="0.3"/>
                                        </g>
                                    </svg>
                                </span>
                        </button>
                        <a href="https://miradapublica.guadalajara.gob.mx/" target="_blank">
                            <img alt="Logo" src="{{ asset('assets/images/mirada_publica.svg') }}"
                                 class="logo-default h-40px h-lg-40px"/>
                            <img alt="Logo" src="{{ asset('assets/images/mirada_publica.svg') }}"
                                 class="logo-sticky h-20px h-lg-25px"/>
                        </a>
                    </div>
                    <div class="d-lg-block" id="kt_header_nav_wrapper">
                        <div class="d-lg-block p-5 p-lg-0" data-kt-drawer="true" data-kt-drawer-name="landing-menu"
                             data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                             data-kt-drawer-width="200px" data-kt-drawer-direction="start"
                             data-kt-drawer-toggle="#kt_landing_menu_toggle" data-kt-swapper="true"
                             data-kt-swapper-mode="prepend"
                             data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav_wrapper'}">
                            <!--begin::Menu-->
                            <div
                                    class="menu menu-column flex-nowrap menu-rounded menu-lg-row menu-title-gray-500 menu-state-title-primary nav nav-flush fs-5 fw-bold"
                                    id="kt_landing_menu">
                                <!--begin::Menu item-->
                                {{--                                    <div class="menu-item">--}}
                                {{--                                        <!--begin::Menu link-->--}}
                                {{--                                        <a class="menu-link nav-link active py-3 px-4 px-xxl-6"--}}
                                {{--                                           href="{{ route('landing') }}"--}}
                                {{--                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">--}}
                                {{--                                            Home--}}
                                {{--                                        </a>--}}
                                {{--                                    </div>--}}
                                <div class="menu-item ">
                                    <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" href="{{route('landing')}}"
                                       data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                        <img alt="Logo" src="{{ asset('assets/images/bandera_contraloria.svg') }}"
                                             class="logo-default h-400px h-lg-100px"/>

                                    </a>
                                </div>

                                <div class="menu-item" style="border-right: 1px solid white">
                                    <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" target="_blank"
                                       href="{{ route('denuncia.registro') }}"
                                       data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                        Presenta tu denuncia
                                    </a>
                                </div>
                                <div class="menu-item" style="border-right: 1px solid white">
                                    <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" target="_blank"
                                       href="{{ route('denuncia.seguimiento') }}"
                                       data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                        Seguimiento de Denuncia
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" href="{{route('estadisticas')}}"
                                       data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Estadísticas</a>
                                </div>
                                <div class="menu-item pt-20">
                                    <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" href="{{route('estadisticas')}}"
                                       data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                        <img alt="Logo" src="{{ asset('assets/images/bandera_logo.svg') }}"
                                             class="logo-default h-400px h-lg-200px"/>

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content" style="padding-top:100px;">
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container">
            @if(isset($response['success']))
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-5">
                            <div class="card-body text-center">
                                <img src="{{ asset('assets/images/logo-gdl.png') }}" width="160xp">
                                <h1 class="text-dark fw-boldest mb-3 fs-4hx">GRACIAS</h1>
                                <img src="{{ asset('assets/images/sonrisa.png') }}" width="80xp">
                                <div class="bg-primary w-500px fs-2x container-success">
                                    Tu <span class="mega-text">denuncia</span> esta siendo <span
                                        class="mega-text">atendida</span>
                                    con el <span class="mega-text">folio</span>
                                    <br><br>
                                    <label class="folio">{{ $object->get_Folio() }}</label>
                                    <br><br>
                                    podras dar seguimiento posteriormente en la <a
                                        href="{{ route('denuncia.seguimiento') }}"> página</a>.
                                </div>
                                <a href="{{ route('landing') }}" class="btn btn-sm btn-primary fs-3">Continuar</a>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                {{ Form::open(array('method' => 'POST', 'id' =>'main_form', 'enctype' => 'multipart/form-data')) }}

                <div class="card">
                    <div class="card-header bg-primary" style="border-bottom-left-radius:30px;border-top-right-radius:30px">
                        <div class="row" style="border-radius: 150px">
                            <div class=" offset-1 col-md-5 align-content-center">
                                <h1 class="text-white fw-boldest mb-3 fs-4hx text-left">DENUNCIA</h1>
{{--                                @if($object->id)--}}
{{--                                    <br>--}}
{{--                                <h3>No. De Carpeta.</h3>--}}
                                <h3>{{$object->report_no}}</h3>
{{--                                @endif--}}
                            </div>
                            <div class="col-md-5 align-content-center pt-4">
                                <div class="text-white-400 fw-bold fs-4 btn_morado text-center" style="height: 50px; padding:10px;  ">
                                    <a href="{{ route('denuncia.seguimiento') }}" class="color-none mt-20">
                                        Consulta aquí el estado de tu denuncia.
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row" >
                        <div class="col-md-10 offset-1 bg-secondary" style="border-bottom-left-radius:30px;border-bottom-right-radius:30px;">
                            <h3 class="text-center text-gray-600">Has sido testigo de un hecho que consideres irregular o has <br>
                                sido víctima de un acto de corrupción <span style="color:#4f458f">¡Denúncialo aquí!</span></h3>
                        </div>
                    </div>

                    @if($object->id)
                        <div class="card-body">
                            <style>
                                .time-line {
                                    display: table;
                                    width: 100%;
                                    text-align: center;
                                }

                                .time-line li {
                                    display: table-cell;
                                }

                                .dot-muted {
                                    background: #a1a5b7 !important;
                                }
                            </style>
                            <ul class="time-line">
                                <li>
                                    <span
                                        class="badge badge-circle badge-light-success btn-color-white fw-bolder p-5 me-3 fs-3">1</span>
                                    {{ $object->reception_date }}
                                    <br>
                                    <br>
                                    <div class="fs-5 fs-lg-3 fw-bolder text-dark">UNIDAD DE DENUNCIAS</div>
                                </li>
                                <li>
                                    <span
                                        class="badge badge-circle badge-light-success btn-color-white fw-bolder p-5 me-3 fs-3 @if($object->get_Area_ID() - 1 < 2) dot-muted @endif">2</span>
                                    @if($object->get_Area_ID() - 1 >= 2)
                                        {{ $object->research_unit->reception_date }}
                                    @endif
                                    <br>
                                    <br>
                                    <div
                                        class="fs-5 fs-lg-3 fw-bolder text-dark @if($object->get_Area_ID() - 1 < 2) text-muted @endif">
                                        UNIDAD DE INVESTIGACIÓN
                                    </div>
                                </li>
                                <li>
                                    <span
                                        class="badge badge-circle badge-light-success btn-color-white fw-bolder p-5 me-3 fs-3 @if($object->get_Area_ID() - 1 < 3) dot-muted @endif">3</span>
                                    @if($object->get_Area_ID() - 1 >= 3)
                                        {{ $object->substance_unit->reception_date }}
                                    @endif
                                    <br>
                                    <br>
                                    <div
                                        class="fs-5 fs-lg-3 fw-bolder text-dark @if($object->get_Area_ID() - 1 < 3) text-muted @endif">
                                        UNIDAD DE SUBSTANCIACIÓN
                                    </div>
                                </li>
                                <li>
                                    <span
                                        class="badge badge-circle badge-light-success btn-color-white fw-bolder p-5 me-3 fs-3 @if($object->get_Area_ID() - 1 < 4) dot-muted @endif">4</span>

                                    @if($object->get_Area_ID() - 1 >= 4)
                                        {{ $object->solving_unit->resolution_date }}
                                    @endif
                                    <br>
                                    <br>
                                    <div
                                        class="fs-5 fs-lg-3 fw-bolder text-dark @if($object->get_Area_ID() - 1 < 4) text-muted @endif">
                                        UNIDAD RESOLUTORA
                                    </div>
                                </li>
                            </ul>
                            <hr>
                        </div>
                    @endif
                    </div>

                @if(is_null($object->id))
                    <br>
                <div class="card card-text">

                    <div class="card-header ">
                        <div class="row">
                            <div class="offset-md-1 col-md-5 text-justify">
                                <h2 class="text-center pb-2" style="color: #4f458f;"><b>Que
                                        <span class="text-success bg-primary" style="border-radius: 50px; padding:8px" >SÍ</span> denunciar aquí</b></h2>                                <ol>
                                    <li>Personas que sean servidores públicos del Gobierno Municipal de Guadalajara.</li>
                                    <li>Personas físicas o morales que:
                                        <ol type="a">
                                            <li>Manejen o apliquen recursos públicos del Municipio de Guadalajara.</li>
                                            <li>Participen en contrataciones públicas del Municipio de Guadalajara.</li>
                                            <li>Participen en transacciones comerciales del Municipio de Guadalajara.</li>
                                            <li>Hayan sido servidores públicos del Gobierno Municipal de Guadalajara.</li>
                                        </ul>
                                    </li>
                                </ol>
                            </div>
                            <div class="offset-md-1 col-md-5 text-start ">
                                <h2 class="text-danger pb-2">Que <span class="text-white " style="border-radius: 50px; padding:8px; background-color: #4f458f">No</span> denunciar aquí</h2>
                                <ol>
                                    <li>Trámites y/o servicios</li>
                                    <li>Asuntos laborales</li>
                                    <li>Conflictos entre particulares</li>
                                    <li>Policías en servicio.</li>
                                    <li>Asuntos relacionados con el Poder Judicial.</li>
                                    <li>Asuntos relacionados con el Poder Legislativo (Congreso y ASEJ).</li>
                                    <li>Asuntos relacionados con autoridades de diferentes niveles de Gobierno (Federal, Estatal o de otros municipios).</li>
                                    <li>Asuntos relacionados con organismos autónomos (IEPC, ITEI, CEDH, TEPJEJ o TJAEJ).</li>
                                    <li>Protección de datos personales.</li>
                                </ol>
                            </div>
                            <div class="row">
                            <div class="col-md-6 offset-3">
                                <a type="" class="btn text-white btn_morado" style="width: 100%;">
                                    Registra tu denuncia
                                </a>
                            </div>
                            </div>

                        </div>
                    </div>
                    </div>
                    <hr>
                @endif
                    <div class="card-body formulario">
                        <div class="row mb-5">
                            {{--                            {% if success %}--}}
                            {{--                            {% include 'snippets/notice.success.html' %}--}}
                            {{--                            {% elif error %}--}}
                            {{--                            {% include 'snippets/notice.error.html' %}--}}
                            {{--                            {% endif %}--}}

                            <div class="col-md-6 ">
                                <div class="col-md-6 col-md-flex-column mb-5 fv-row  " style="border-top-right-radius: 10px;background: #ECECEC !important;border-bottom-right-radius: 10px;">
                                    <p class="bg-secondary text-uppercase" style="margin: 20px;padding: 10px;">Datos del denunciante</p>
                                </div>
                                <div class="row">

                                    <div class="mb-5 fv-row col-md-6">
                                        <p
                                            class=" text-gray fs-5 fw-bold mb-2 form-check form-switch form-check-custom form-check-solid">
                                            {{ Form::label('name', '¿Tu denuncia es anónima?') }}
                                        </p>
                                    </div>
                                    <div class="mb-5 fv-row col-md-1">
                                        <label
                                                class=" mb-2" style="color:#D9214E">No   </label>                                      </label>
                                    </div>
                                    <div class="mb-5 fv-row col-md-2">
                                        <p
                                            class=" text-gray fs-5 fw-bold mb-2 form-check form-switch form-check-custom form-check-solid">
                                            {{ Form::checkbox('anonima', null, '', array('class' => 'form-check-input mx-4')) }}
                                        </p>
                                    </div>
                                    <div class="mb-5 fv-row col-md-1">
                                        <label
                                                class=" mb-2" style="color:#D9214E">Si   </label>                                      </label>
                                    </div>



                                    <div class="mb-5 fv-row anonimo">
                                        <label class="required fs-5 fw-bold mb-2">
                                            {{ Form::label('name', 'NOMBRE (S)') }}
                                        </label>
                                        {{ Form::text('informer_name', $object->informer_name, array('class' => 'form-control form-control-solid required', 'required' => 'required')) }}
                                    </div>
{{--<!----}}
                                    <div class="mb-5 col-md-6 fv-row anonimo">
                                        <label class="required fs-5 fw-bold mb-2">
                                            {{ Form::label('name', 'APELLIDO PATERNO') }}
                                        </label>
                                        {{ Form::text('informer_last_name', $object->informer_last_name, array('class' => 'form-control form-control-solid required', 'required' => 'required')) }}
                                    </div>
                                    <div class="mb-5 col-md-6 fv-row anonimo">
                                        <label class="fs-5 fw-bold mb-2">
                                            {{ Form::label('name', 'APELLIDO MATERNO') }}
                                        </label>
                                        {{ Form::text('informer_last_name_2', $object->informer_last_name_2, array('class' => 'form-control form-control-solid')) }}
                                    </div>
                                    <div class="mb-5 col-md-12 fv-row">
                                        <label class="required fs-5 fw-bold mb-2">
                                            {{ Form::label('informer_gender', 'Sexo') }}
                                        </label>
                                        {{ Form::select('informer_gender', $gender, $object->informer_gender, array('class' => 'form-select form-select-solid required', 'required' => 'required')) }}
                                    </div>
                                    <div class="col-md-12 mb-5 fv-row">
                                        <label class="required fs-5 fw-bold mb-2">
                                            {{ Form::label('informer_email', 'Correo Electrónico') }}
                                        </label>
                                        {{ Form::email('informer_email', $object->informer_email, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                                    </div>
                                    <div class="mb-5 fv-row col-md-6">
                                        <p
                                                class=" text-gray fs-5 fw-bold mb-2 form-check form-switch form-check-custom form-check-solid">
                                            {{ Form::label('name', '¿AUTORIZA RECIBIR LAS NOTIFICACIONES POR CORREO ELECTRÓNICO??') }}
                                        </p>
                                    </div>
                                    <div class="mb-5 fv-row col-md-1">
                                        <label
                                                class=" mb-2" style="color:#D9214E">Si   </label>                                      </label>
                                    </div>
                                    <div class="mb-5 fv-row col-md-2">
                                        <p
                                                class=" text-gray fs-5 fw-bold mb-2 form-check form-switch form-check-custom form-check-solid">
                                            {{ Form::checkbox('notificaciones', null, '', array('class' => 'form-check-input mx-4')) }}
                                        </p>
                                    </div>
                                    <div class="mb-5 fv-row col-md-1">
                                        <label
                                                class=" mb-2" style="color:#D9214E">No   </label>                                      </label>
                                    </div>


                                    <div class="col-md-12 mb-5 fv-row notificacion  " >
                                        <label class="fs-5 fw-bold mb-2">
                                            {{ Form::label('informer_phone', 'Teléfono') }}
                                        </label>
                                        {{ Form::text('informer_phone', $object->informer_phone, array('class' => 'form-control form-control-solid phone')) }}
                                    </div>

                                    <div class="mb-5 fv-row notificacion">
                                        <label class="fs-5 fw-bold mb-2">
                                            {{ Form::label('informer_address', 'Dirección') }}
                                        </label>
                                        {{ Form::textarea('informer_address', $object->informer_address, array('class' => 'form-control form-control-solid', 'rows' => '5')) }}

                                    </div>
                                    <div class="col-md-6 col-md-flex-column mb-5 fv-row  " style="border-top-right-radius: 10px;background: #ECECEC !important;border-bottom-right-radius: 10px;">
                                        <p class="bg-secondary text-uppercase" style="padding-top: 10px;padding-left: 10px;">Datos del denunciado</p>
                                    </div>
                                    <div class="mb-5 fv-row col-md-8">
                                        <label
                                            class="fs-5 fw-bold mb-2 form-check form-switch form-check-custom form-check-solid">
                                            {{ Form::label('name', 'Desconozco al denunciado') }}
                                        </label>
                                    </div>


                                    <div class="mb-5 fv-row col-md-1">
                                        <label
                                                class=" mb-2" style="color:#D9214E">No   </label>                                      </label>
                                    </div>
                                    <div class="mb-5 fv-row col-md-2">
                                        <p
                                                class=" text-gray fs-5 fw-bold mb-2 form-check form-switch form-check-custom form-check-solid">
                                            {{ Form::checkbox('unknown', null, '', array('class' => 'form-check-input mx-4')) }}
                                        </p>
                                    </div>
                                    <div class="mb-5 fv-row col-md-1">
                                        <label
                                                class=" mb-2" style="color:#D9214E">Si   </label>                                      </label>
                                    </div>

                                    <div class="flex-column mb-5 fv-row hide unknown_hide">
                                        <label class="required fs-5 fw-bold mb-2">
                                            {{ Form::label('accused_description', 'Por favor proporciona una descripcion del denunciado') }}
                                        </label>
                                        {{ Form::textarea('accused_description', $object->accused_description, array('class' => 'form-control form-control-solid required', 'rows' => '5')) }}
                                    </div>
                                    <div class="flex-column mb-5 fv-row unknown">
                                        <label class="required fs-5 fw-bold mb-2">
                                            {{ Form::label('accused_name', 'NOMBRE (S) ') }}
                                        </label>
                                        {{ Form::text('accused_name', $object->accused_name, array('class' => 'form-control form-control-solid required', 'required' => 'required')) }}
                                    </div>
                                    <div class="mb-5 col-md-6 fv-row unknown">
                                        <label class="required fs-5 fw-bold mb-2">
                                            {{ Form::label('name', 'Apellido Paterno') }}
                                        </label>
                                        {{ Form::text('acused_last_name', $object->acused_last_name, array('class' => 'form-control form-control-solid required', 'required' => 'required')) }}
                                    </div>
                                    <div class="mb-5 col-md-6 fv-row unknown">
                                        <label class="fs-5 fw-bold mb-2">
                                            {{ Form::label('name', 'Apellido Materno') }}
                                        </label>
                                        {{ Form::text('acused_last_name_2', $object->acused_last_name_2, array('class' => 'form-control form-control-solid')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 center-container">
                                <img src="{{ asset('assets/images/bg-write.jpg') }}" width="90%">
                            </div>
                            <div class="col-md-3 mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('accused_gender', 'Sexo ') }}
                                </label>
                                {{ Form::select('accused_gender', $gender_d, $object->accused_gender, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                            </div>
                            <div class="col-md-6 mb-5 fv-row">
                                <label class="fs-5 fw-bold mb-2">
                                    {{ Form::label('accused_charge', 'Cargo') }}
                                </label>
                                {{ Form::text('accused_charge', $object->accused_charge, array('class' => 'form-control form-control-solid')) }}
                            </div>
                            <div class="col-md-3 mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('accused_dependency', 'Dependencia ') }}
                                </label>
                                {{ Form::select('accused_dependency', $dependence, $object->accused_dependency, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-5 fv-row">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('reported_place', 'DESCRIBE EL LUGAR DONDE OCURRIERON LOS HECHOS') }}
                                    </label>
                                    {{ Form::textarea('reported_place', $object->reported_place, array('class' => 'form-control form-control-solid', 'required' => 'required', 'rows' => '5')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-5 fv-row">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('reported_event', 'DESCRIBE LOS HECHOS DENUNCIADOS') }}
                                    </label>
                                    {{ Form::textarea('reported_event', $object->reported_event, array('class' => 'form-control form-control-solid', 'required' => 'required', 'rows' => '5')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-5 fv-row">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('date_reported', 'FECHA QUE OCURRIERON LOS HECHOS DENUNCIADOS') }}
                                    </label>
                                    {{ Form::date('date_reported', $object->reported_event, array('class' => ' date form-control form-control-solid', 'required' => 'required', 'rows' => '5')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-5 fv-row">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('time_reported', 'HORA QUE OCURRIERON LOS HECHOS DENUNCIADOS') }}
                                    </label>
                                    {{ Form::time('time_reported', $object->reported_event, array('class' => ' date form-control form-control-solid', 'required' => 'required', 'rows' => '5')) }}
                                </div>
                            </div>

                            <div class="mb-5 col-md-12 fv-row">

                                <label for="evidencia">REFUERZA TU DENUNCIA ADJUNTANDO ARCHIVOS DE PRUEBA (no más de 3MB de peso)</label>
                                <br>
                                <br>
                                <input type="file"
                                       id="evidencia" name="evidencia">
                                <br>
                                <br>
                                <p class="pt-3" style="line-height: 1.2px; color: gray">
                                    <span class="text-danger">*</span> Espacios necesarios para recibir su denuncia
                                </p>


                            </div>

                            @if(!isset($readonly))
                                <div class="mb-5 fv-row">
                                    <label
                                        class="fs-5 fw-bold mb-2 form-check form-check-custom form-check-solid fw-bold ">
                                        <input type="checkbox" class="form-check-input mx-2" name="check"
                                               required="required"> He leído y acepto el
                                        <a href="{{ url('politicas') }}" class="mx-1"> Aviso de Privacidad</a>.
                                    </label>
                                </div>
                            </div>
                                <div class="mb-5 col-md-6 fv-row">
                                    <button type="submit" class="btn btn-primary" style="width: 100%">
                                        <b>Registrar  tu denuncia </b>
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            @endif
        </div>
    </div>
<div class="mb-0 pt-10">
    <!--begin::Curve top-->
    <div class="landing-dark-bg ">
        <div class="container">
            <div class="row text-white">
                <div class="col-md-4 text-center pt-5">
                    <a href="https://www.facebook.com/GobiernoJalisco" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/facebook.svg" width="50px">
                    </a>
                    <a href="https://twitter.com/GobiernoJalisco" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/twitter.svg" width="50px">
                    </a>
                    <a href="https://www.instagram.com/gobjalisco/" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/instagram.svg" width="50px">
                    </a>
                    <a href="https://www.youtube.com/channel/UCQBDDUradzK_8kVbLHvzVeg" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/youtube.svg" width="50px">
                    </a>
                    <p class="pt-5">
                        <b> @ 2022 Gobierno de Guadalajara</b>
                        <br>
                        <a href="https://guadalajara.gob.mx/aviso-privacidad.pdf" target="_blank" class="text-white">Aviso de Privacidad</a>
                        <br>
                        <a href="https://www.youtube.com/watch?v=AcaFAD-FZdM&feature=youtu.be" target="_blank" class="text-white">Aviso de Privacidad en Lengua de Señas Mexicana
                            <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/descargar_archivos.svg" height="15px"></a>
                    </p>
                </div>
                <div class="col-md-3 text-center pt-1">
                    <img alt="Logo" src="{{ asset('assets/images/curva.svg') }}" width="100%">
                </div>
                <div class="col-md-2 text-justify pt-5">
                    <ul style="  list-style-type: none;">
                        <li class="pt-3"><b>Mapa del Sitio</b></li>
                        <a href="https://guadalajara.gob.mx/gobierno" target="_blank" class="text-white"><li class="pt-3">Gobierno</li></a>
                        <a href="https://tramites.guadalajara.gob.mx/tramites-y-servicios" target="_blank" class="text-white"><li class="pt-3">Tramites</li></a>
                        <a href="https://transparencia.guadalajara.gob.mx/" target="_blank" class="text-white"><li class="pt-3">Transparencia</li></a>
                    </ul>
                </div>
                <div class="col-md-3 text-justify pt-5">
                    <ul style="  list-style-type: none;">
                        <li class="pt-3"><b>Contacto</b></li>
                        <li class="pt-3"> <b>Dirección:</b> <br> Hidalgo 400, Col Centro, CP: 44100
                        </li>
                        <li class="pt-3"><b>Teléfono:</b> <br> 3837-4400
                        </li>
                        <li class="pt-3"><b>Escríbenos a:</b> <br> contacto@guadalajara.gob.mx</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@if(isset($readonly))
    <style>
        input,
        .form-control.form-control-solid,
        .form-select.form-select-solid {
            background: #FFF !important;
            border: none;
            pointer-events: none;
            padding-left: 0;
        }
    </style>
@endif

<script src="{{ asset('assets/js/plugins.bundle.js') }}"></script>
<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/auth/general.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
<script>

    $(".formulario").hide();
    $('.phone').mask('00-00-00-00-00');
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
    $( ".btn_morado" ).click(function() {
        $(".formulario").show();
        $(".card-text").hide();
    });

    $(document).on('change', '[name="anonima"]', function () {
        if ($(this).is(':checked')) {
            $('.anonimo').hide();
            $('.anonimo').find('input, textarea, select').each(function () {
                $(this).removeAttr('required');
            });
        } else {
            $('.anonimo').show();
            $('.anonimo').find('input, textarea, select').each(function () {
                if ($(this).hasClass('required')) {

                    $(this).attr('required', 'required');
                }
            });
        }
    });
    $(document).on('change', '[name="notificaciones"]', function () {
        if ($(this).is(':checked')) {
            $('.notificacion').hide();
            $('.notificacion').find('input, textarea, select').each(function () {
                $(this).removeAttr('required');
            });
        } else {
            $('.notificacion').show();
            $('.notificacion').find('input, textarea, select').each(function () {
                if ($(this).hasClass('required')) {

                    $(this).attr('required', 'required');
                }
            });
        }
    });

    $(document).on('change', '[name="unknown"]', function () {
        if ($(this).is(':checked')) {
            $('.unknown').hide();
            $('.unknown').find('input, textarea, select').each(function () {
                $(this).removeAttr('required');
            });

            $('.unknown_hide').show();
            $('.unknown_hide').find('input, textarea, select').each(function () {
                $(this).attr('required', 'required');
            });
        } else {
            $('.unknown').show();
            $('.unknown').find('input, textarea, select').each(function () {
                if ($(this).hasClass('required')) {

                    $(this).attr('required', 'required');
                }
            });

            $('.unknown_hide').hide();
            $('.unknown_hide').find('input, textarea, select').each(function () {
                $(this).removeAttr('required');
            });
        }
    });
</script>
</body>
<!--end::Body-->
</html>

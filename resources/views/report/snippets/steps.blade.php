<ul class="steper-nav">
    @foreach($steps as $index => $step)
        <li class="steper-item @if($index == $step_selected) active @endif">

            <div>{{$index}}</div>
            {!! $step !!}

        </li>
    @endforeach
</ul>

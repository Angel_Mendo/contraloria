<style>
    .file-container {
        width: 80%;
        height: 90%;
        position: absolute;
        box-shadow: 0px 0px 20px #30313a;
        background: #ffffff;
        bottom: 0;
        z-index: 1;
        display: none;
        margin: 0 10%;
    }

    #table-file-selector tbody tr td,
    #table-file-selector thead tr th {
        padding-left: 10px;
    }

    #table-file-selector thead {
        background: #FC637D;
        color: #ffffff;
        font-weight: bold;
    }
</style>

<div class="file-container">
    <div class="card">
        <div class="card-header">
            <div class="card-title fw-bold">
                ARCHIVOS
            </div>
            <div class="card-toolbar">
                <a href="#" class="btn-close-file">
                    <i class="fa fa-times fs-3"></i>
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('report.file') }}" method="POST"
                          enctype="multipart/form-data"
                          class="dropzone"
                          id="dropzone-files">
                        {{ csrf_field() }}
                        <input type="hidden" name="step" value="{{$step_selected}}">
                        <input type="hidden" name="folio" value="{{$folio}}">
                    </form>
                </div>
                <div class="col-md-12 mt-4">
                    <table class="table table-striped align-middle table-row-dashed fs-6 gy-5" id="table-file-selector">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Archivo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($files as $file)
                            <tr>
                                <td>
                                    {{$file->created_at}}
                                </td>
                                <td>
                                    <a href="{{ $file->path }}" download>
                                        {{$file->name}}
                                    </a>
                                </td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

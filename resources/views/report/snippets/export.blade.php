<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<table>
    <tbody>
    <tr>
        <td colspan="68" style="border: none;" height="60rem">
        </td>
    </tr>
    <!--tr>
        <td colspan="68" style="border: none;" height="62rem">
            El presente Registro de Procedimientos de Investigación y Responsabilidad Administrativa fue elaborado con
            base en los criterios establecidos en la Consulta Jurídica 008/2018, misma que fue aprobada por el Pleno del
            Instituto de Transparencia, Información Pública y Protección de Datos Personales del Estado de Jalisco
            (ITEI) en la Cuadragésima Cuarta Sesión Ordinaria celebrada el 12 de diciembre del año 2018.
            <br>* De conformidad a lo establecido en el artículo 113 fracciones IX, X y XI de la Ley General de
            Transparencia y Acceso a la Información Pública, en concordancia con el artículo 17, numeral 1, fracciones
            I, inciso g, IV y V de la Ley de Transparencia y Acceso a la Información Pública del Estado de Jalisco y sus
            Municipios y en la Sexta Sesión Extraordinaria 2018 del Comité de Transparencia del H. Ayuntamiento de
            Guadalajara celebrada el pasado 14 de marzo del 2018, la información de los procedimientos que se encuentran
            en proceso, hasta en tanto no causen estado, encuadra en los supuestos de reserva.
            <br>* Se elimina el nombre de la persona denunciante por considerarse información confidencial, de
            conformidad a
            lo previsto en los artículos 64 y 91 de la Ley General de Responsabilidades Administrativas, en concordancia
            con el artículo 21 de la Ley de Transparencia y Acceso a la Información Pública del Estado de Jalisco y sus
            Municipios; y en el artículo 3, numeral 1, fracción IX de la Ley de Protección de Datos Personales en
            Posesión de Sujetos Obligados del Estado de Jalisco y sus Municipios.
            <br>* En el supuesto de no encontrarse responsabilidad en la persona servidora pública, una vez emitida la
            resolución, este queda en potestad de ejercer su derecho a la protección de sus datos personales, a través
            de la oposición al tratamiento o cancelación de sus datos personales en este registro específico, de
            conformidad a lo previsto en el artículo 46, fracción IV de la Ley de Protección de Datos Personales en
            Posesión de Sujetos Obligados del Estado de Jalisco y sus Municipios.
            <br>* Se limita la publicación de la información a lo contenido en el artículo 53 de la Ley General del
            Sistema
            Nacional Anticorrupción y párrafo cuarto del artículo 27 de la Ley General de Responsabilidades
            Administrativas
        </td>
    </tr-->
    <tr>
        <td colspan="68" height="15rem">
            <b>Fecha de Actualización: {{ date('d-m-Y') }}</b>
        </td>
    </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th colspan="68" height="20rem"
            style="background: #16365C; color: #ffffff; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>REGISTRO DE RESPONSABILIDAD ADMINISTRATIVA</b>
        </th>
    </tr>
    <tr>
        <th colspan="21" height="20rem"
            style="background: #B1A0C7; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>UNIDAD DE DENUNCIAS</b>
        </th>
        <th colspan="13" height="20rem"
            style="background: #31869B; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>UNIDAD DE INVESTIGACIÓN</b>
        </th>
        <th colspan="19" height="20rem"
            style="background: #E26B0A; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>UNIDAD DE SUBSTANCIACIÓN</b>
        </th>
        <th colspan="15" height="20rem"
            style="background: #92D050; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>UNIDAD RESOLUTORA</b>
        </th>
    </tr>
    <tr>
        <th colspan="21" height="20rem"
            style="background: #B1A0C7; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>DENUNCIAS POR FALTAS ADMISTRATIVAS</b>
        </th>
        <th colspan="13" height="20rem"
            style="background: #31869B; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>PROCEDIMIENTO DE INVESTIGACIÓN ADMINSITRATIVA</b>
        </th>
        <th colspan="11" height="20rem"
            style="background: #E26B0A; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>PROCEDIMIENTO DE RESPONSABILIDAD ADMINISTRATIVA</b>
        </th>
        <th colspan="3" height="20rem"
            style="background: #DFDF0D; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>POR FALTAS ADMINISTRATIVAS GRAVES</b>
        </th>
        <th colspan="5" height="20rem"
            style="background: #FFC000; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>POR FALTAS ADMINISTRATIVAS NO GRAVES</b>
        </th>
        <th colspan="15" height="20rem"
            style="background: #92D050; text-align: center; font-weight: bold; border: 4rem solid #000000;">
            <b>ETAPA DE RESOLUCIÓN</b>
        </th>
    </tr>
    <tr>
        <th height="40rem" style="background: #16365C; color: #ffffff; text-align: center;border: 4rem solid #000000;">
            <b>No.</b>
        </th>
        <th height="40rem" width="55rem" style="background: #E4DFEC; text-align: center;border: 4rem solid #000000;">
            <b>ÁREA DONDE SE ENCUENTRA EL EXPEDIENTE AL CORTE <br>DE INFORMACIÓN</b>
        </th>
        <th height="40rem" width="15rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>EJERCICIO</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>PERIODO DE <br>ACTUALIZACIÓN</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE RECEPCIÓN DE <br>DENUNCIA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>NÚMERO DE DENUNCIA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>MECANISMO DE RECEPCIÓN</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>TIPO DE DENUNCIANTE</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>SEXO DEL DENUNCIANTE*</b>
        </th>
        <th height="40rem" width="35rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>PERSONA DENUNCIANTE*</b>
        </th>
        <th height="40rem" width="35rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>PERSONA DENUNCIADA*</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>DEPENDENCIA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>CARGO</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>TIPO FALTA DENUNCIADA</b>
        </th>
        <th height="40rem" width="55rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>HECHOS DENUNCIADOS</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>PLAZO DE ATENCIÓN*<br>ISO 9000:2015</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>TRÁMITE DE UNIDAD DE <br>DENUNCIAS</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>ESTATUS</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE ESTATUS</b>
        </th>
        <th height="40rem" width="25rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>SUMATORIA DE DÍAS DE <br>ATENCIÓN</b>
        </th>
        <th height="40rem" width="55rem" style="background: #E4DFEC; text-align: center; border: 4rem solid #000000;">
            <b>
                OBSERVACIONES UNIDAD DE DENUNCIAS<br>
                <small>(En caso de ser necesario realizar notas metodológicas)</small>
            </b>
        </th>

        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                NÚMERO DE INVESTIGACIÓN
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                FECHA DE RECEPCIÓN <br>UNIDAD DE INVESTIGACIÓN
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                FECHA DE ACUERDO <br>RADICACIÓN*
            </b>
        </th>
        <th height="40rem" width="35rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                ORIGEN DE INVESTIGACIÓN*
            </b>
        </th>
        <th height="40rem" width="55rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                HECHOS INVESTIGADOS
            </b>
        </th>
        <th height="40rem" width="35rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                ABOGADO RESPONSABLE <br>UNIDAD DE INVESTIGACIÓN*
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                ESTATUS DE INVESTIGACIÓN
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                CALIFICACIÓN
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                FECHA DE CALIFICACIÓN
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                FECHA DE ESTATUS
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                SUMATORIA DE DÍAS DE <br>ATENCIÓN
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                FECHA DE IMPUGNACIÓN <br>CALIFICACIÓN
            </b>
        </th>
        <th height="40rem" width="55rem" style="background: #B7DEE8; text-align: center; border: 4rem solid #000000;">
            <b>
                OBSERVACIONES UNIDAD DE INVESTIGACIÓN<br>
                <small>(En caso de ser necesario señalar notas metodológicas aplicables)</small>
            </b>
        </th>

        {{--      Unidad de Investigación      --}}
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE RECEPCIÓN DE <br>IPRA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>ESTATUS</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE ADMISIÓN*</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>NÚMERO DE PROCEDIMIENTO <br>RESPONSABILIDAD <br>ADMINISTRATIVA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>
                TIPO DE FALTA <br>ADMINISTRATIVA
            </b>
        </th>
        <th height="40rem" width="35rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>
                PRESUNTO RESPONSABLE*
            </b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>SEXO DEL PRESUNTO <br>RESPONSABLE</b>
        </th>
        <th height="40rem" width="35rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>ABOGADO RESPONSABLE <br>SUBSTANCIACIÓN*</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE ACUERDO DE <br>EMPLAZAMIENTO</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE <br>EMPLAZAMIENTO</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FCD5B4; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE AUDIENCIA<br>INICIAL</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FFFF99; text-align: center; border: 4rem solid #000000;">
            <b>EN CASO DE FALTA GRAVE<br>FECHA DE ENVIO AL TJA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FFFF99; text-align: center; border: 4rem solid #000000;">
            <b>NÚMERO DE EXPEDIENTE<br> TJA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FFFF99; text-align: center; border: 4rem solid #000000;">
            <b>SALA<br> TJA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FFFFCC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE ACUERDO DE <br>ADMISIÓN DE PRUEBAS</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FFFFCC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE ALEGATOS</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FFFFCC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE REMISIÓN AL <br>ÁREA DE RESOLUCIÓN</b>
        </th>
        <th height="40rem" width="25rem" style="background: #FFFFCC; text-align: center; border: 4rem solid #000000;">
            <b>SUMATORIA DE DÍAS DE <br>ATENCIÓN</b>
        </th>
        <th height="40rem" width="55rem" style="background: #FFFFCC; text-align: center; border: 4rem solid #000000;">
            <b>
                OBSERVACIONES UNIDAD SUBSTANCIADORA*<br>
                <small>
                    (En caso de aplicar señalar si se presentó algún medio de impugnación o notas metodológicas
                    aplicables)
                </small>
            </b>
        </th>

        {{--    Unidad Resolutora    --}}
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>ANALISIS DE PRA*</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE CIERRE DE <br>INSTRUCCIÓN</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE RESOLUCIÓN</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>TIPO DE RESOLUCIÓN*</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>TIPO DE SANCIÓN*</b>
        </th>
        <th height="40rem" width="55rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>ESPECIFICACIÓN DE LA SANCIÓN<br><small>(NOTA METODOLÓGICA)</small></b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>ARTICULO DE LA <br>NORMATIVIDAD <br>INFRINGIDA*</b>
        </th>
        <th height="40rem" width="35rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>PERSONA SANCIONADA<br>*HASTA QUE CAUSE ESTADO SE PUBLICA</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>SEXO DEL RESPONSABLE</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA DE NOTIFICACIÓN <br>RESOLUCIÓN</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA INFORME DE <br>EJECUCIÓN*</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>FECHA QUE CAUSA <br>ESTADO</b>
        </th>
        <th height="40rem" width="25rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>SUMATORIA DE DÍAS DE <br>ATENCIÓN</b>
        </th>
        <th height="40rem" width="55rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>HIPERVINCULO A LA RESOLUCIÓN <br><small>(Versión Pública)</small></b>
        </th>
        <th height="40rem" width="55rem" style="background: #D8E4BC; text-align: center; border: 4rem solid #000000;">
            <b>OBSERVACIONES UNIDAD RESOLUTORA*<br>
                <small>(En caso de aplicar señalar si se presentó algún medio de impugnación o notas metodológicas
                    aplicables)
                </small>
            </b>
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($objects as $object)
        <tr>
            <td height="20rem"
                style="background: #16365C; color: #ffffff; text-align: center; border: 4rem solid #000000;">
                <b>{{ $object->id }}</b>
            </td>
            <td style="background: #E4DFEC; border: 4rem solid #000000; text-transform: capitalize;">
                <b>{{ $object->get_Area() }}</b>
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                <b>{{ $object->exercise }}</b>
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ date('m-Y', strtotime($object->update_period)) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ date('d/m/Y', strtotime($object->reception_date)) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->report_no }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000; {{ ($object->mechanism) ? '': 'color: red;' }}">
                {{ ($object->mechanism) ? $object->mechanism->name : 'Falta dato' }}
                {{--                {{ $object->get_ReportType($object->report_type) }}--}}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->get_InformerType($object->informer_type) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->get_Gender($object->informer_gender) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->informer_name }} {{ $object->informer_last_name }} {{ $object->informer_last_name_2 }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->accused_name }} {{ $object->accused_last_name }} {{ $object->accused_last_name_2 }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->accused_dependency }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->accused_charge }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->get_ReportType($object->report_type) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->reported_event }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->attention_period }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->get_UnitProcessing($object->unit_processing) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->get_ReporStatus($object->status) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ date('d/m/Y', strtotime($object->status_date)) }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">

            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ $object->commentary }}
            </td>

            {{--      Unidad de Investigación      --}}
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->research_no : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? date('d/m/Y', strtotime($object->research_unit->reception_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? date('d/m/Y', strtotime($object->research_unit->settlement_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->get_ResearchOrigin() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->investigated_event : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->responsible_lawyer : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->get_ResearchStatus() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->qualification : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? date('d/m/Y', strtotime($object->research_unit->qualification_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? date('d/m/Y', strtotime($object->research_unit->status_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->sum_Days() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? date('d/m/Y', strtotime($object->research_unit->challenge_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->research_unit) ? $object->research_unit->note : '' }}
            </td>

            {{--      Unidad de Substanciación      --}}
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->reception_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->get_SubstanceStatus() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->admission_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->process_no : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->get_TypeFoul() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->presumed_responsible : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->get_Gender($object->substance_unit->presumed_responsible_gender) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->responsible_lawyer : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->agreement_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->emplacement_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->audience_initial_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->tja_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->tja_no : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->tja_room : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->test_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->allegation_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? date('d/m/Y', strtotime($object->substance_unit->resolution_area_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->sum_Days() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->substance_unit) ? $object->substance_unit->note : '' }}
            </td>

            {{--      Unidad Resolutora      --}}
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->get_Analysis() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? date('d/m/Y', strtotime($object->solving_unit->closing_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? date('d/m/Y', strtotime($object->solving_unit->resolution_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->get_ResolutionType() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->get_sanctionType() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->sanction_description : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->article : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->sanctioned_person : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->get_Gender($object->solving_unit->sanctioned_gender) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? date('d/m/Y', strtotime($object->solving_unit->notification_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? date('d/m/Y', strtotime($object->solving_unit->exec_report_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? date('d/m/Y', strtotime($object->solving_unit->causing_date)) : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->sum_Days() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->get_Link() : '' }}
            </td>
            <td style="text-align: center; border: 4rem solid #000000;">
                {{ ($object->solving_unit) ? $object->solving_unit->note : '' }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</html>

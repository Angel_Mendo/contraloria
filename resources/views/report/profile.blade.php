@extends('base.backend')

@section('title')
    Denuncias
@endsection

@section('subtitle')
    Perfil
@endsection


@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-5">
                        <div class="col-md-6">
                            <div class="d-flex flex-column mb-5 fv-row">
                                <h4>Datos del Denunciante</h4>
                            </div>
                            <div class="row">
                                <div class="mb-5 fv-row anonimo">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('name', 'Nombre') }}
                                    </label>
                                    <p>
                                        {{$object->informer_name}}
                                    </p>
                                </div>

                                <div class="mb-5 col-md-6 fv-row anonimo">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('name', 'Apellido Paterno') }}
                                    </label>
                                    <p>
                                        {{$object->informer_last_name}}
                                    </p>
                                </div>
                                <div class="mb-5 col-md-6 fv-row anonimo">
                                    <label class="fs-5 fw-bold mb-2">
                                        {{ Form::label('name', 'Apellido Materno') }}
                                    </label>
                                    <p>
                                        {{$object->informer_last_name_2}}
                                    </p>
                                </div>
                                <div class="mb-5 fv-row anonimo">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('informer_gender', 'Sexo') }}
                                    </label>
                                    <p>
                                        {{($object->get_Gender($object->informer_gender))}}

                                    </p>
                                </div>
                                <div class="col-md-6 mb-5 fv-row">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('informer_email', 'Correo Electronico') }}
                                    </label>
                                    <p>
                                        {{$object->informer_email}}
                                    </p>
                                </div>
                                <div class="col-md-6 mb-5 fv-row">
                                    <label class="fs-5 fw-bold mb-2">
                                        {{ Form::label('informer_phone', 'Teléfono') }}
                                    </label>
                                    {{$object->informer_phone}}
                                </div>
                                <div class="mb-5 fv-row">
                                    <label class="fs-5 fw-bold mb-2">
                                        {{ Form::label('informer_address', 'Dirección') }}
                                    </label>
                                    <p>
                                        {{$object->informer_address}}
                                    </p>
                                </div>
                                <div class="d-flex flex-column mb-5 fv-row">
                                    <hr>
                                    <h4>Datos del Denunciado</h4>
                                </div>
{{--                                <div class="mb-5 fv-row">--}}
{{--                                    <label--}}
{{--                                            class="fs-5 fw-bold mb-2 form-check form-switch form-check-custom form-check-solid">--}}
{{--                                        {{ Form::label('name', 'Desconozco al denunciado') }}--}}
{{--                                        {{ Form::checkbox('unknown', null, '', array('class' => 'form-check-input mx-4')) }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
                                <div class="flex-column mb-5 fv-row hide unknown_hide">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('accused_description', 'Por favor proporciona una descripcion del denunciado') }}
                                    </label>
                                    <p>
                                        {{$object->accused_description}}
                                    </p>
                                </div>
                                <div class="flex-column mb-5 fv-row unknown">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('accused_name', 'Nombre del denunciado') }}
                                    </label>
                                    <p>
                                        {{$object->accused_name}}
                                    </p>
                                </div>
                                <div class="mb-5 col-md-6 fv-row unknown">
                                    <label class="required fs-5 fw-bold mb-2">
                                        {{ Form::label('name', 'Apellido Paterno') }}
                                    </label>
                                    <p>
                                        {{$object->acused_last_name}}
                                    </p>
                                </div>
                                <div class="mb-5 col-md-6 fv-row unknown">
                                    <label class="fs-5 fw-bold mb-2">
                                        {{ Form::label('name', 'Apellido Materno') }}
                                    </label>
                                    {{$object->acused_last_name_2}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 center-container">
                            <img src="{{ asset('assets/images/bg-write.jpg') }}" width="90%">
                        </div>
                        <div class="col-md-6 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('accused_gender', 'Sexo ') }}
                            </label>
                            {{$object->accused_gender}}
                        </div>
                        <div class="col-md-3 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('accused_charge', 'Cargo') }}
                            </label>
                            <p>
                                {{$object->accused_charge}}
                            </p>
                        </div>
                        <div class="col-md-3 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('accused_dependency', 'Dependencia ') }}
                            </label>
                            <p>
                                {{$object->get_dependency($object->accused_dependency)}}
                            </p>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-8">
                            <div class="mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('reported_place', 'Lugar de los Hechos') }}
                                </label>
                                <p>
                                    {{$object->reported_place}}
                                </p>
                            </div>

                            <div class="mb-5 fv-row">
                                <label class="required fs-5 fw-bold mb-2">
                                    {{ Form::label('reported_event', 'Hechos Denunciados') }}
                                </label>
                               <p> {{$object->reported_event}}</p>
                            </div>
                        </div>
                        <div class="col-md-2 center-container">
                            <img src="{{ asset('assets/images/logo-gdl-white.jpg') }}" width="200px">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('extrajs')
@endsection


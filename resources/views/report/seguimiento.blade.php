<!DOCTYPE html>
<html lang="es">
<!--begin::Head-->
<head>
    <title>Contraloria En Datos</title>
    <meta name="description" content="Contraloria En Datos"/>
    <meta name="keywords"
          content="Denuncias"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta charset="utf-8"/>
    <meta property="og:locale" content="es_MX"/>
    <meta property="og:title"
          content="Contraloria En Datos - Dash"/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom.app.css') }}" rel="stylesheet" type="text/css"/>

    <style>
        .btn-morado{
            background-color: #4f458f !important;
        }
    </style>
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="bg-body">
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <div class="mb-0" id="home">
        <div class="bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom landing-dark-bg">
            <div class="landing-header" style="height: 70px !important;" data-kt-sticky="true" data-kt-sticky-name="landing-header"
                 data-kt-sticky-offset="{default: '200px', lg: '300px'}">
                <div class="container">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center flex-equal">
                            <button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none"
                                    id="kt_landing_menu_toggle">
                                <span class="svg-icon svg-icon-2hx">
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                         viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <rect fill="#000000" x="4" y="5" width="16" height="3" rx="1.5"/>
                                            <path
                                                    d="M5.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 Z M5.5,10 L18.5,10 C19.3284271,10 20,10.6715729 20,11.5 C20,12.3284271 19.3284271,13 18.5,13 L5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z"
                                                    fill="#000000" opacity="0.3"/>
                                        </g>
                                    </svg>
                                </span>
                            </button>
                            <a href="https://miradapublica.guadalajara.gob.mx/" target="_blank">
                                <img alt="Logo" src="{{ asset('assets/images/mirada_publica.svg') }}"
                                     class="logo-default h-40px h-lg-40px"/>
                                <img alt="Logo" src="{{ asset('assets/images/mirada_publica.svg') }}"
                                     class="logo-sticky h-20px h-lg-25px"/>
                            </a>
                        </div>
                        <div class="d-lg-block" id="kt_header_nav_wrapper">
                            <div class="d-lg-block p-5 p-lg-0" data-kt-drawer="true" data-kt-drawer-name="landing-menu"
                                 data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                                 data-kt-drawer-width="200px" data-kt-drawer-direction="start"
                                 data-kt-drawer-toggle="#kt_landing_menu_toggle" data-kt-swapper="true"
                                 data-kt-swapper-mode="prepend"
                                 data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav_wrapper'}">
                                <!--begin::Menu-->
                                <div
                                        class="menu menu-column flex-nowrap menu-rounded menu-lg-row menu-title-gray-500 menu-state-title-primary nav nav-flush fs-5 fw-bold"
                                        id="kt_landing_menu">
                                    <!--begin::Menu item-->
                                    {{--                                    <div class="menu-item">--}}
                                    {{--                                        <!--begin::Menu link-->--}}
                                    {{--                                        <a class="menu-link nav-link active py-3 px-4 px-xxl-6"--}}
                                    {{--                                           href="{{ route('landing') }}"--}}
                                    {{--                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">--}}
                                    {{--                                            Home--}}
                                    {{--                                        </a>--}}
                                    {{--                                    </div>--}}
                                    <div class="menu-item ">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" href="{{route('landing')}}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                            <img alt="Logo" src="{{ asset('assets/images/bandera_contraloria.svg') }}"
                                                 class="logo-default h-400px h-lg-100px"/>

                                        </a>
                                    </div>

                                    <div class="menu-item" style="border-right: 1px solid white">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" target="_blank"
                                           href="{{ route('denuncia.registro') }}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                            Presenta tu denuncia
                                        </a>
                                    </div>
                                    <div class="menu-item" style="border-right: 1px solid white">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" target="_blank"
                                           href="{{ route('denuncia.seguimiento') }}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                            Seguimiento de Denuncia
                                        </a>
                                    </div>
                                    <div class="menu-item">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" href="{{route('estadisticas')}}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Estadísticas</a>
                                    </div>
                                    <div class="menu-item pt-20">
                                        <a class="menu-link nav-link py-3 px-4 px-xxl-6 text-white" href="{{route('estadisticas')}}"
                                           data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">
                                            <img alt="Logo" src="{{ asset('assets/images/bandera_logo.svg') }}"
                                                 class="logo-default h-400px h-lg-200px"/>

                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--begin::Authentication - Sign-in -->
    <div
        class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
        <!--begin::Content-->
        <div class="d-flex col-md-10 offset-1 flex-center flex-column flex-column-fluid  bg-primary"
             style="border-bottom-left-radius: 30px;border-top-right-radius: 30px;margin-top: 100px;">
            <div class="col-md-6 m-10" style="background-color:#4f458f;">
                <h1 class="text-white fw-boldest p-5 fs-1hx text-center">ESTADO DE TU DENUNCIA</h1>
            </div>
        </div>
        </div>

            <div class="w-lg-400px bg-body p-10 p-lg-15 mx-auto">
                <!--begin::Form-->
                <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form"
                      action="{{ route('denuncia.seguimiento') }}"
                      method="GET">
{{--                {!! csrf_field() !!}--}}
                <!--begin::Heading-->
                    <div class="fv-row mb-10 text-center">
                        <h1 class=" fw-bolder text-family" style="font-size:4rem;">
                            Folio
                        </h1>
                        <input class="form-control form-control-lg form-control-solid text-center" type="text" name="folio"
                               autocomplete="off"/>
                    </div>
                    <!--end::Input group-->
                    <!--begin::Actions-->
                    <div class="fv-plugins-message-container invalid-feedback text-center p-5">
                        <div data-field="password" data-validator="notEmpty">
                            @if(isset($message))
                                <p class="text-family" style="font-size: 20px" >{{ $message['message']  }}</p>
                            @endif

                            @if(isset($_GET['message']))
                                    <p class="text-family" style="font-size: 20px" >
                                        {{$_GET['message']}}
                                    </p>

                            @endif

                        </div>
                    </div>

                    <div class="text-center">
                        <!--begin::Submit button-->
                        <button type="submit" id="kt_sign_in_submit" class="btn btn-lg  btn-morado  text-white w-100 mb-5">
                            <span class="indicator-label">Buscar denuncia</span>
                            <span class="indicator-progress">Espera...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                        <!--end::Submit button-->
                        <!--begin::Separator-->
                    </div>
                    <!--end::Actions-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Content-->
        <!--begin::Footer-->
<div class="mb-0 pt-10">
    <!--begin::Curve top-->
    <div class="landing-dark-bg ">
        <div class="container">
            <div class="row text-white">
                <div class="col-md-4 text-center pt-5">
                    <a href="https://www.facebook.com/GobiernoJalisco" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/facebook.svg" width="50px">
                    </a>
                    <a href="https://twitter.com/GobiernoJalisco" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/twitter.svg" width="50px">
                    </a>
                    <a href="https://www.instagram.com/gobjalisco/" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/instagram.svg" width="50px">
                    </a>
                    <a href="https://www.youtube.com/channel/UCQBDDUradzK_8kVbLHvzVeg" class="p-3">
                        <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/youtube.svg" width="50px">
                    </a>
                    <p class="pt-5">
                        <b> @ 2022 Gobierno de Guadalajara</b>
                        <br>
                        <a href="https://guadalajara.gob.mx/aviso-privacidad.pdf" target="_blank" class="text-white">Aviso de Privacidad</a>
                        <br>
                        <a href="https://www.youtube.com/watch?v=AcaFAD-FZdM&feature=youtu.be" target="_blank" class="text-white">Aviso de Privacidad en Lengua de Señas Mexicana
                            <img src="https://miradapublica.guadalajara.gob.mx/assets/img/landing/descargar_archivos.svg" height="15px"></a>
                    </p>
                </div>
                <div class="col-md-3 text-center pt-1">
                    <img alt="Logo" src="{{ asset('assets/images/curva.svg') }}" width="100%">
                </div>
                <div class="col-md-2 text-justify pt-5">
                    <ul style="  list-style-type: none;">
                        <li class="pt-3"><b>Mapa del Sitio</b></li>
                        <a href="https://guadalajara.gob.mx/gobierno" target="_blank" class="text-white"><li class="pt-3">Gobierno</li></a>
                        <a href="https://tramites.guadalajara.gob.mx/tramites-y-servicios" target="_blank" class="text-white"><li class="pt-3">Tramites</li></a>
                        <a href="https://transparencia.guadalajara.gob.mx/" target="_blank" class="text-white"><li class="pt-3">Transparencia</li></a>
                    </ul>
                </div>
                <div class="col-md-3 text-justify pt-5">
                    <ul style="  list-style-type: none;">
                        <li class="pt-3"><b>Contacto</b></li>
                        <li class="pt-3"> <b>Dirección:</b> <br> Hidalgo 400, Col Centro, CP: 44100
                        </li>
                        <li class="pt-3"><b>Teléfono:</b> <br> 3837-4400
                        </li>
                        <li class="pt-3"><b>Escríbenos a:</b> <br> contacto@guadalajara.gob.mx</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

        <!--end::Footer-->
    </div>

    <!--end::Authentication - Sign-in-->
</div>
<!--end::Main-->
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{% static 'assets/js/plugins.bundle.js' %}"></script>
<script src="{% static 'assets/js/scripts.bundle.js' %}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{% static 'assets/js/auth/general.js' %}"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
<style>
    .card {
        border: none;
    }
</style>
</body>
<!--end::Body-->
</html>

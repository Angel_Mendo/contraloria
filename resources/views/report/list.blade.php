@extends('base.backend')

@section('title')
    Denuncias
@endsection

@section('subtitle')
    Lista
@endsection


@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">

            <div class="card">
                <div class="card-header border-0 pt-6">
                    <div class="card-title card-title-table">
                        <div class="d-flex align-items-center position-relative my-1">
                            <span class="position-absolute ms-6">
                                <i class="fa fa-search fs-6"></i>
                            </span>
                            <input type="text" data-kt-table-filter="search"
                                   class="form-control form-control-solid form-control-sm w-250px ps-14"
                                   placeholder="Buscar..."/>
                        </div>
                    </div>
                    <div class="card-toolbar">
                        <div class="d-flex mx-2 justify-content-end" data-kt-user-table-toolbar="base">
                            <div class="d-flex align-items-center position-relative my-1">
                                <span class="position-absolute ms-6">
                                    <i class="fa fa-calendar fs-6"></i>
                                </span>
                                <input type="text" name="range_created_at"
                                       class="form-control form-control-sm form-control-solid w-250px ps-14 date_range">
                                <a href="{{ route('report.download') }}"
                                   class="position-relative ms-2 btn btn-sm btn-light btn-download">
                                    <i class="fa fa-download fs-6"></i> Descargar
                                </a>
                                <a href="#"
                                   class="position-relative ms-2 btn btn-sm btn-light" data-bs-toggle="modal"
                                   data-bs-target="#modal_upload">
                                    <i class="fa fa-upload fs-6"></i> Subir
                                </a>
                            </div>

                        </div>
                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                            <a href="{{ route('report.register', ['pk' => 00000, 'step' => 1]) }}"
                               class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Nuevo
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0 table-responsive">
                    <table class="table table-striped align-middle table-row-dashed fs-6 gy-5" id="main-table">
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script>
        $('input[name="range_created_at"]').on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;

            $('#main-table').DataTable().ajax.url('{{ route('api.report.list') }}?user_id={{ session()->get('pk') }}&date_start=' + start.format('YYYY-MM-DD') + '&date_end=' + end.format('YYYY-MM-DD')).load();
            $('.btn-download').attr('href', '{{ route('report.download') }}?user_id={{ session()->get('pk') }}&date_start=' + start.format('YYYY-MM-DD') + '&date_end=' + end.format('YYYY-MM-DD'));
        });

        var data_Columns = [
            {data: 'id', name: 'id', 'title': '#'},
            {data: 'report_no', name: 'report_no', 'title': 'No. Denuncia'},
            {data: 'exercise', name: 'exercise', 'title': 'Ejercicio'},
            {data: 'mechanism', name: 'mechanism', 'title': 'Mecanismo de recepción'},
            {data: 'area', name: 'area', 'title': 'Area actual'},
            {data: 'reception_date', name: 'reception_date', 'title': 'Fecha de recepción'},
            // {data: 'status', name: 'status', 'title': 'Estatus'},
            {data: 'actions', name: 'actions', 'title': ''},
            // {data: 'action', name: 'action'},
        ];
        $('#main-table').setDataTable('{{ route('api.report.list') }}?user_id={{ session()->get('pk') }}', data_Columns);


        {{--$('#main-table').setDataTable('{{ route('api.report.list') }}');--}}

        $(document).on('click', '.btn-ulpoad-file', function (e) {
            $('.message-success').empty();
            $('.message-error').empty();

            var data = new FormData(document.getElementById('form_upload'));
            var url = $('#form_upload').attr('action')
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'html',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.message-success').text('Carga completa con exito!')
                }
            }).done(function () {
                $('.message-success').text('Carga completa con exito!')
            }).fail(function (x, y, z) {
                console.log(x)
                $('.message-error').text(x.responseText);
            });
            e.preventDefault();
        })
    </script>
@endsection


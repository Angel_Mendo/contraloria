@extends('base.backend')

@section('title')
    Denuncias
@endsection

@section('subtitle')
    Lista
@endsection

@php($research_origin = \App\Models\ResearchUnit::RESEARCH_ORIGIN)
@php($research_status = \App\Models\ResearchUnit::RESEARCH_STATUS)
@php($research_qualification = \App\Models\ResearchUnit::QUALIFICATION)
@php($user = \Illuminate\Support\Facades\Auth::user())


@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card">

                {{ Form::open(array('method' => 'POST', 'id' =>'main_form', 'enctype' => 'multipart/form-data')) }}
                {{ csrf_field() }}
                <div class="card-body pt-4 ">
                    <div class="card-header">
                        @include('report.snippets.steps')
                    </div>


                    <div class="row">
                        <div class="col-md-12 mb-5 mt-5 fv-row">
                            <h5>
                                PROCEDIMIENTO DE INVESTIGACIÓN ADMINSITRATIVA
                            </h5>
                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('area', 'Área Donde se Encuentra el Expediente:') }}
                            </label>
                            {{ Form::text('area', $report_unit->get_Area(), array('class' => 'form-control form-control-solid', 'required' => 'required', 'disabled' => 'disabled')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('reception_date', 'Fecha de Recepción de Denuncia:') }}
                            </label>
                            {{ Form::date('reception_date', ($report_unit->reception_date) ? date('Y-m-d', strtotime($report_unit->reception_date)) : date('Y-m-d'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('exercise', 'Ejercicio:') }}
                            </label>
                            {{ Form::text('exercise', ($report_unit->exercise) ? $report_unit->exercise : date('Y'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('update_period', 'Periodo de Actualización:') }}
                            </label>
                            {{ Form::month('update_period', ($report_unit->update_period) ? date('Y-m', strtotime($report_unit->update_period)) : date('Y-m'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('report_no', 'Número de Denuncia:') }}
                            </label>
                            {{ Form::text('report_no', $report_unit->get_ReportNo(), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        {{--            End Cabeceras            --}}
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('research_no', 'Número de Investigación:') }}
                            </label>
                            {{ Form::text('research_no', $object->research_no, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('reception_date', 'Fecha de Recepción:') }}
                            </label>
                            {{ Form::date('reception_date', ($object->reception_date) ? date('Y-m-d', strtotime($object->reception_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('settlement_date', 'Fecha de Acuerdo de Radicación:') }}
                            </label>
                            {{ Form::date('settlement_date', ($object->settlement_date) ?  date('Y-m-d', strtotime($object->settlement_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('research_origin', 'Origen de Investigación:') }}
                            </label>
                            {{ Form::select('research_origin', $research_origin, $object->research_origin, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-12 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('investigated_event', 'Hechos Investigados:') }}
                            </label>
                            {{ Form::textarea('investigated_event', $object->investigated_event, array('class' => 'form-control form-control-solid', 'required' => 'required', 'rows' => '4')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-4 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('responsible_lawyer', 'Abogado Responsable Unidad de Investigación:') }}
                            </label>
                            {{ Form::text('responsible_lawyer', $object->responsible_lawyer, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('research_status', 'Estatus de Investigación:') }}
                            </label>
                            {{ Form::select('research_status', $research_status, $object->research_status, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('status_date', 'Fecha de Estatus:') }}
                            </label>
                            {{ Form::date('status_date', ($object->status_date) ? date('Y-m-d', strtotime($object->status_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('qualification', 'Determinación:') }}
                            </label>
                            {{ Form::select('qualification', $research_qualification, $object->qualification, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class=" fs-5 fw-bold mb-2">
                                {{ Form::label('qualification_date', 'Fecha de Calificación:') }}
                            </label>
                            {{ Form::date('qualification_date', ($object->qualification_date) ? date('Y-m-d', strtotime($object->qualification_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('sum_days', 'Sumatoria Días de Atención:') }}
                            </label>
                            {{ Form::number('sum_days', $object->sum_Days(), array('class' => 'form-control form-control-solid text-center', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('challenge_date', 'Fecha Impugnación Calificación:') }}
                            </label>
                            {{ Form::date('challenge_date', ($object->challenge_date) ? date('Y-m-d', strtotime($object->challenge_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('note', 'Observaciones Unidad de Investigación:') }}
                            </label>
                            {{ Form::textarea('note', $object->note, array('class' => 'form-control form-control-solid', 'rows' => '4')) }}
                        </div>
                    </div>
                    <div class="card-footer">
                        @if($user->is_supervisor or $user->role == 1)
                            <a href="{{ route('report.register', ['pk' => $folio, 'step' => $step - 1]) }}"
                               class="btn btn-success">
                                <i class="fa fa-angle-left"></i> Anterior
                            </a>

                            <a href="#" class="btn btn-success btn-next mx-1 float-end"
                               data-url="{{ route('report.register', ['pk' =>$folio, 'step' => 3]) }}">
                                Siguiente <i class="fa fa-angle-right"></i>
                            </a>
                        @endif

                        @if($user->role == 1 or $user->role == 3)
                            <button type="submit" class="btn btn-success btn-save float-end">
                                Guardar <i class="fa fa-save"></i>
                            </button>
                        @endif
                        <button type="button" class="btn btn-dark btn-file mx-1 float-end">
                            <i class="fa fa-files"></i> Archivos
                        </button>
                    </div>
                </div>
                {{Form::close()}}
                @include('report.snippets.file_container')
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css"/>
    <script>
        $(document).on('click', '.btn-next', function (e) {
            if ($('.btn-save').length) {
                $('#main_form').append($('<input>').attr('type', 'hidden').attr('name', 'next_step').val(true));
                $('.btn-save').trigger('click');
            } else {
                window.location.href = $(this).attr('data-url');
            }

            e.preventDefault();
        });

        $(document).on('click', '.btn-prev', function (e) {
            $('#main_form').append($('<input>').attr('type', 'hidden').attr('name', 'prev_step').val(true));
            $('.btn-save').trigger('click');
            e.preventDefault();
        });

        $(document).on('click', '.btn-file', function () {
            $('.file-container').slideDown(400);
        });

        $(document).on('click', '.btn-close-file', function () {
            $('.file-container').slideUp(400);
        });

        $("form#dropzone-files").dropzone({
            paramName: 'file',
            url: '{{ route('report.file') }}',
            dictDefaultMessage: 'Arrastra los archivos aquí.',
            //acceptedFiles: "image/*",
        });
    </script>
@endsection


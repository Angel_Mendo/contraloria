@extends('base.backend')

@section('title')
    Denuncias
@endsection

@section('subtitle')
    Lista
@endsection

@php($type_foul = \App\Models\SubstanceUnit::TYPE_FOUL)
@php($status = \App\Models\SubstanceUnit::SUBSTANCE_STATUS)
@php($user = \Illuminate\Support\Facades\Auth::user())


@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card">

                {{ Form::open(array('method' => 'POST', 'id' =>'main_form', 'enctype' => 'multipart/form-data')) }}
                {{ csrf_field() }}
                <div class="card-body pt-4 ">
                    <div class="card-header">
                        @include('report.snippets.steps')
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-5 mt-5 fv-row">
                            <h5>
                                PROCEDIMIENTO DE RESPONSABILIDAD ADMINISTRATIVA
                            </h5>
                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('area', 'Área Donde se Encuentra el Expediente:') }}
                            </label>
                            {{ Form::text('area', $report_unit->get_Area(), array('class' => 'form-control form-control-solid', 'required' => 'required', 'disabled' => 'disabled')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('reception_date', 'Fecha de Recepción de Denuncia:') }}
                            </label>
                            {{ Form::date('reception_date', ($report_unit->reception_date) ? date('Y-m-d', strtotime($report_unit->reception_date)) : date('Y-m-d'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('exercise', 'Ejercicio:') }}
                            </label>
                            {{ Form::text('exercise', ($report_unit->exercise) ? $report_unit->exercise : date('Y'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('update_period', 'Periodo de Actualización:') }}
                            </label>
                            {{ Form::month('update_period', ($report_unit->update_period) ? date('Y-m', strtotime($report_unit->update_period)) : date('Y-m'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('report_no', 'Número de Denuncia:') }}
                            </label>
                            {{ Form::text('report_no', $report_unit->get_ReportNo(), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        {{--            End Cabeceras            --}}
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('reception_date', 'Fecha de Recepción de IPRA:') }}
                            </label>
                            {{ Form::date('reception_date', ($object->reception_date) ? date('Y-m-d', strtotime($object->reception_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('status', 'Estatus:') }}
                            </label>
                            {{ Form::select('status', $status, $object->status, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('admission_date', 'Fecha de estatus:') }}
                            </label>
                            {{ Form::date('admission_date', ($object->admission_date) ? date('Y-m-d', strtotime($object->admission_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('process_no', 'Número de Procedimiento:') }}
                            </label>
                            {{ Form::text('process_no', $object->process_no, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
{{--                        <div class="col-md-2 mb-5 fv-row">--}}
{{--                            <label class="required fs-5 fw-bold mb-2">--}}
{{--                                {{ Form::label('type_foul', 'Tipo de Falta:') }}--}}
{{--                            </label>--}}
{{--                            {{ Form::select('type_foul', $type_foul, $object->type_foul, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}--}}
{{--                        </div>--}}
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('presumed_responsible', 'Presunto Responsable:') }}
                            </label>
                            {{ Form::text('presumed_responsible', $object->presumed_responsible, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('presumed_responsible_gender', 'Sexo del Presunto Responsable:') }}
                            </label>
                            {{ Form::select('presumed_responsible_gender', $gender, $object->presumed_responsible_gender, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('responsible_lawyer', 'Abogado Responsable Substanciación:') }}
                            </label>
                            {{ Form::text('responsible_lawyer', $object->responsible_lawyer, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('agreement_date', 'Fecha Acuerdo Emplazamiento:') }}
                            </label>
                            {{ Form::date('agreement_date', ($object->agreement_date) ? date('Y-m-d', strtotime($object->agreement_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('emplacement_date', 'Fecha Emplazamiento:') }}
                            </label>
                            {{ Form::date('emplacement_date', ($object->emplacement_date) ? date('Y-m-d', strtotime($object->emplacement_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('audience_initial_date', 'Fecha Audiencia Inicial:') }}
                            </label>
                            {{ Form::date('audience_initial_date', ($object->audience_initial_date) ? date('Y-m-d', strtotime($object->audience_initial_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-12">
                            <hr>
                            <h6>
                                POR FALTAS ADMINISTRATIVAS GRAVES <br>
                            </h6>
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('tja_date', 'Fecha Envio al TJA:') }}
                            </label>
                            {{ Form::date('tja_date', ($object->tja_date) ? date('Y-m-d', strtotime($object->tja_date)) : '', array('class' => 'form-control form-control-solid')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('tja_no', 'Número de Expediente TJA:') }}
                            </label>
                            {{ Form::text('tja_no', $object->tja_no, array('class' => 'form-control form-control-solid')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('tja_room', 'Sala TJA:') }}
                            </label>
                            {{ Form::text('tja_room', $object->tja_room, array('class' => 'form-control form-control-solid')) }}
                        </div>

                        <div class="col-md-12">
                            <hr>
                            <h6>
                                POR FALTAS ADMINISTRATIVAS NO GRAVES
                            </h6>
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('test_date', 'Fecha Acuerdo Admisión Pruebas:') }}
                            </label>
                            {{ Form::date('test_date', ($object->test_date) ? date('Y-m-d', strtotime($object->test_date)) : '', array('class' => 'form-control form-control-solid')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('allegation_date', 'Fecha de Alegatos:') }}
                            </label>
                            {{ Form::date('allegation_date', ($object->allegation_date) ? date('Y-m-d', strtotime($object->allegation_date)) : '', array('class' => 'form-control form-control-solid')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('resolution_area_date', 'Fecha Remisión Area Resolución:') }}
                            </label>
                            {{ Form::date('resolution_area_date', ($object->resolution_area_date) ? date('Y-m-d', strtotime($object->resolution_area_date)) : '', array('class' => 'form-control form-control-solid')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('sum_days', 'Sumatoria Días de Atención:') }}
                            </label>
                            {{ Form::number('sum_days', $object->sum_Days(), array('class' => 'form-control form-control-solid text-center', 'required' => 'required')) }}
                        </div>


                        <div class="col-md-12 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('note', 'Observaciones Unidad Substanciadora:') }}
                            </label>
                            {{ Form::textarea('note', $object->note, array('class' => 'form-control form-control-solid', 'rows' => '4')) }}
                        </div>

                    </div>
                    <div class="card-footer">
                        @if($user->is_supervisor or $user->role == 1)
                            <a href="{{ route('report.register', ['pk' => $folio, 'step' => $step - 1]) }}"
                               class="btn btn-success"
                               data-url="{{ route('report.register', ['pk' => $folio, 'step' => 3]) }}">
                                <i class="fa fa-angle-left"></i> Anterior
                            </a>

                            <a href="#" class="btn btn-success btn-next mx-1 float-end"
                               data-url="{{ route('report.register', ['pk' => $folio, 'step' => 4]) }}">
                                Siguiente <i class="fa fa-angle-right"></i>
                            </a>
                        @endif
                        @if($user->role == 1 or $user->role == 4)
                            <button type="submit" class="btn btn-success btn-save float-end">
                                Guardar <i class="fa fa-save"></i>
                            </button>
                        @endif
                        <button type="button" class="btn btn-dark btn-file mx-1 float-end">
                            <i class="fa fa-files"></i> Archivos
                        </button>
                    </div>
                </div>
                {{Form::close()}}
                @include('report.snippets.file_container')
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css"/>
    <script>
        $(document).on('click', '.btn-next', function (e) {
            if ($('.btn-save').length) {
                $('#main_form').append($('<input>').attr('type', 'hidden').attr('name', 'next_step').val(true));
                $('.btn-save').trigger('click');
            } else {
                window.location.href = $(this).attr('data-url');
            }

            e.preventDefault();
        });

        $(document).on('click', '.btn-prev', function (e) {
            $('#main_form').append($('<input>').attr('type', 'hidden').attr('name', 'prev_step').val(true));
            $('.btn-save').trigger('click');
            e.preventDefault();
        });

        $(document).on('click', '.btn-file', function () {
            $('.file-container').slideDown(400);
        });

        $(document).on('click', '.btn-close-file', function () {
            $('.file-container').slideUp(400);
        });

        $("form#dropzone-files").dropzone({
            paramName: 'file',
            url: '{{ route('report.file') }}',
            dictDefaultMessage: 'Arrastra los archivos aquí.',
            //acceptedFiles: "image/*",
        });

    </script>
@endsection


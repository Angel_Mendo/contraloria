@extends('base.backend')

@section('title')
    Denuncias
@endsection

@section('subtitle')
    Lista
@endsection

@php($resolution_type = App\Models\SolvingUnit::RESOLUTION_TYPE)
@php($sanction_type = App\Models\SolvingUnit::SANCTION_TYPE)
@php($analysis = App\Models\SolvingUnit::ANALYSIS_PRA)
@php($user = \Illuminate\Support\Facades\Auth::user())


@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card">

                {{ Form::open(array('method' => 'POST', 'id' =>'main_form', 'enctype' => 'multipart/form-data')) }}
                {{ csrf_field() }}
                <div class="card-body pt-4 ">
                    <div class="card-header">
                        @include('report.snippets.steps')
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-5 mt-5 fv-row">
                            <h5>
                                ETAPA DE RESOLUCIÓN
                            </h5>
                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('area', 'Área Donde se Encuentra el Expediente:') }}
                            </label>
                            {{ Form::text('area', $report_unit->get_Area(), array('class' => 'form-control form-control-solid', 'required' => 'required', 'disabled' => 'disabled')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('reception_date', 'Fecha de Recepción de Denuncia:') }}
                            </label>
                            {{ Form::date('reception_date', ($report_unit->reception_date) ? date('Y-m-d', strtotime($report_unit->reception_date)) : date('Y-m-d'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('exercise', 'Ejercicio:') }}
                            </label>
                            {{ Form::text('exercise', ($report_unit->exercise) ? $report_unit->exercise : date('Y'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('update_period', 'Periodo de Actualización:') }}
                            </label>
                            {{ Form::month('update_period', ($report_unit->update_period) ? date('Y-m', strtotime($report_unit->update_period)) : date('Y-m'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('report_no', 'Número de Denuncia:') }}
                            </label>
                            {{ Form::text('report_no', $report_unit->get_ReportNo(), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        {{--            End Cabeceras            --}}
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('analysis', 'Analisis del PRA:') }}
                            </label>
                            {{ Form::select('analysis', $analysis, $object->analysis, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('closing_date', 'Fecha de Cierre de :') }}
                            </label>
                            {{ Form::date('closing_date', ($object->closing_date) ? date('Y-m-d', strtotime($object->closing_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('resolution_type', 'Tipo de Resolución:') }}
                            </label>
                            {{ Form::select('resolution_type', $resolution_type, $object->resolution_type, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('resolution_date', 'Fecha de Resolución:') }}
                            </label>
                            {{ Form::date('resolution_date', ($object->resolution_date) ? date('Y-m-d', strtotime($object->resolution_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('sanction_type', 'Tipo de Sanción:') }}
                            </label>
                            {{ Form::select('sanction_type', $sanction_type, $object->sanction_type, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('sanction_description', 'Especificación de la Sanción (Nota Metodológica):') }}
                            </label>
                            {{ Form::textarea('sanction_description', $object->sanction_description, array('class' => 'form-control form-control-solid', 'required' => 'required', 'rows' => '4')) }}
                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('article', 'Articulo Normatividad Infrigida:') }}
                            </label>
                            {{ Form::text('article', $object->article, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-4 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('sanctioned_person', 'Persona Sancionada:') }}
                            </label>
                            {{ Form::text('sanctioned_person', $object->sanctioned_person, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('sanctioned_gender', 'Sexo del Responsable:') }}
                            </label>
                            {{ Form::select('sanctioned_gender', $gender, $object->sanctioned_gender, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('notification_date', 'Fecha Notificación Resolución:') }}
                            </label>
                            {{ Form::date('notification_date', ($object->notification_date) ? date('Y-m-d', strtotime($object->notification_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('exec_report_date', 'Fecha Informe de Ejecución:') }}
                            </label>
                            {{ Form::date('exec_report_date', ($object->exec_report_date) ? date('Y-m-d', strtotime($object->exec_report_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('causing_date', 'Fecha Que Causa Estado:') }}
                            </label>
                            {{ Form::date('causing_date', ($object->causing_date) ? date('Y-m-d', strtotime($object->causing_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('sum_days', 'Sumatoria Días de Atención:') }}
                            </label>
                            {{ Form::number('sum_days', $object->sum_Days(), array('class' => 'form-control form-control-solid text-center', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('note', 'Observaciones Unidad Resolutora:') }}
                            </label>
                            <br>
                            <small>
                                (En caso de aplicar señalar si se presentó algún medio de impugnación o notas
                                metodológicas aplicables)
                            </small>
                            {{ Form::textarea('note', $object->note, array('class' => 'form-control form-control-solid', 'rows' => '4')) }}
                        </div>

                    </div>
                    <div class="card-footer">
                        @if($user->is_supervisor or $user->role == 1)
                            <a href="{{ route('report.register', ['pk' => $folio, 'step' => $step - 1]) }}"
                               class="btn btn-success">
                                <i class="fa fa-angle-left"></i> Anterior
                            </a>
                        @endif
                        @if($user->role == 1 or $user->role == 5)
                            <button type="submit" class="btn btn-success btn-save float-end">
                                Guardar <i class="fa fa-save"></i>
                            </button>
                        @endif
                        <button type="button" class="btn btn-dark btn-file mx-1 float-end">
                            <i class="fa fa-files"></i> Archivos
                        </button>
                    </div>
                </div>
                {{Form::close()}}
                @include('report.snippets.file_container')
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css"/>
    <script>
        $(document).on('click', '.btn-next', function (e) {
            $('#main_form').append($('<input>').attr('type', 'hidden').attr('name', 'next_step').val(true));
            $('.btn-save').trigger('click');
            e.preventDefault();
        });

        $(document).on('click', '.btn-prev', function (e) {
            $('#main_form').append($('<input>').attr('type', 'hidden').attr('name', 'prev_step').val(true));
            $('.btn-save').trigger('click');
            e.preventDefault();
        });

        $(document).on('click', '.btn-file', function () {
            $('.file-container').slideDown(400);
        });

        $(document).on('click', '.btn-close-file', function () {
            $('.file-container').slideUp(400);
        });

        $("form#dropzone-files").dropzone({
            paramName: 'file',
            url: '{{ route('report.file') }}',
            dictDefaultMessage: 'Arrastra los archivos aquí.',
            //acceptedFiles: "image/*",
        });
    </script>
@endsection


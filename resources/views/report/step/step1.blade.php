@extends('base.backend')

@section('title')
    Denuncias
@endsection

@section('subtitle')
    Lista
@endsection

@php($informer_type = \App\Models\Report::INFORMER_TYPE)
@php($unit_processing = \App\Models\Report::UNIT_PROCESSING)
@php($report_type = \App\Models\Report::REPORT_TYPE)
@php($status = \App\Models\Report::STATUS)
@php($user = \Illuminate\Support\Facades\Auth::user())


@section('content')
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-fluid">
            <div class="card">
                {{ Form::open(array('method' => 'POST', 'id' =>'main_form', 'enctype' => 'multipart/form-data')) }}
                {{ csrf_field() }}
                <div class="card-body pt-4">
                    <div class="card-header">
                        @include('report.snippets.steps')
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-5 mt-5 fv-row">
                            <h5>
                                DENUNCIAS POR FALTAS ADMISTRATIVAS
                            </h5>
                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('type_expediente', 'Tipo de Expediente:') }}
                            </label>
                            {{ Form::select('type_expediente', $type_expediente, $object->type_expediente, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}

                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('area', 'Área Donde se Encuentra el Expediente:') }}
                            </label>
                            {{ Form::text('area', $object->get_Area(), array('class' => 'form-control form-control-solid', 'required' => 'required', 'disabled' => 'disabled')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('reception_date', 'Fecha de Recepción de Denuncia:') }}
                            </label>
                            {{ Form::date('reception_date', ($object->reception_date) ? date('Y-m-d', strtotime($object->reception_date)) : date('Y-m-d'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('exercise', 'Ejercicio:') }}
                            </label>
                            {{ Form::text('exercise', ($object->exercise) ? $object->exercise : date('Y'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('update_period', 'Periodo de Actualización:') }}
                            </label>
                            {{ Form::month('update_period', ($object->update_period) ? date('Y-m', strtotime($object->update_period)) : date('Y-m'), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('report_no', 'Número de Denuncia:') }}
                            </label>
                            {{ Form::text('report_no', $object->get_ReportNo(), array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('informer_name', 'Persona Denunciante:') }}
                            </label>
                            {{ Form::text('informer_name', ($object->informer_name) ? $object->informer_name : 'Anónimo', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('informer_last_name', 'Apellido Paterno:') }}
                            </label>
                            {{ Form::text('informer_last_name', ($object->informer_last_name) ? $object->informer_last_name : 'Anónimo', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('informer_last_name_2', 'Apellido Materno:') }}
                            </label>
                            {{ Form::text('informer_last_name_2', $object->informer_last_name_2, array('class' => 'form-control form-control-solid')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('mechanism_id', 'Mecanismo de Recepción:') }}
                            </label>
                            {{ Form::select('mechanism_id', $mechanism, $object->mechanism_id, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('informer_type', 'Tipo de Denunciante:') }}
                            </label>
                            {{ Form::select('informer_type', $informer_type, $object->informer_type, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('informer_gender', 'Sexo del Denunciante:') }}
                            </label>
                            {{ Form::select('informer_gender', $gender, ($object->informer_gender) ? $object->informer_gender : 4, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('accused_name', 'Persona Denunciada:') }}
                            </label>
                            {{ Form::text('accused_name', ($object->accused_name) ? $object->accused_name : 'No identificado', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('acused_last_name', 'Apellido Paterno:') }}
                            </label>
                            {{ Form::text('acused_last_name', ($object->acused_last_name) ? $object->acused_last_name : 'No identificado', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('acused_last_name_2', 'Apellido Materno:') }}
                            </label>
                            {{ Form::text('acused_last_name_2', ($object->acused_last_name_2) ? $object->acused_last_name_2 : 'No identificado', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('accused_charge', 'Cargo:') }}
                            </label>
                            {{ Form::text('accused_charge', $object->accused_charge, array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('accused_dependency', 'Dependencia:') }}
                            </label>
                            {{ Form::select('accused_dependency', $dependence, $object->accused_dependency, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('report_type', 'Tipo de Falta Denunciada:') }}
                            </label>
                            {{ Form::select('report_type', $report_type, $object->report_type, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-4 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('type_conducta', 'Tipo de Conductarm -rf .git

Fuente: https://www.iteramos.com/pregunta/12618/git---como-eliminar-el-rastreo-de-git-de-un-proyecto') }}
                            </label>
                            {{ Form::text('type_conducta', $object->type_conducta, array('class' => 'form-control form-control-solid', 'required' => 'required', 'disabled' => 'disabled')) }}
                        </div>
                        <div class="col-md-12 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('reported_place', 'Lugar de los Hechos:') }}
                            </label>
                            {{ Form::textarea('reported_place', $object->reported_place, array('class' => 'form-control form-control-solid', 'rows' => '4')) }}
                        </div>
                        <div class="col-md-6 mb-5 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('reported_event', 'Hechos Denunciados:') }}
                            </label>
                            {{ Form::textarea('reported_event', $object->reported_event, array('class' => 'form-control form-control-solid', 'required' => 'required', 'rows' => '4')) }}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('attention_period', 'Plazo de Atención ISO 9000:2015:') }}
                            </label>
                            {{ Form::select('attention_period', $attention_period, $object->attention_period, array('class' => 'form-select form-select-solid text-center', 'required' => 'required')) }}
                        </div>

                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('attention_period', 'Trámite de Unidad de Denuncias:') }}
                            </label>
                            {{ Form::select('unit_processing', $unit_processing, $object->unit_processing, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('status', 'Estatus:') }}
                            </label>
                            {{ Form::select('status', $status, $object->status, array('class' => 'form-select form-select-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-2 mb-5 mt-6 fv-row">
                            <label class="required fs-5 fw-bold mb-2">
                                {{ Form::label('status_date', 'Fecha de Status:') }}
                            </label>
                            {{ Form::date('status_date', ($object->status_date) ? date('Y-m-d', strtotime($object->status_date)) : '', array('class' => 'form-control form-control-solid', 'required' => 'required')) }}
                        </div>
                        <div class="col-md-12 mb-5 fv-row">
                            <label class="fs-5 fw-bold mb-2">
                                {{ Form::label('commentary', 'Observaciones Unidad de Denuncias:') }}
                            </label>
                            {{ Form::textarea('commentary', $object->commentary, array('class' => 'form-control form-control-solid', 'rows' => '4')) }}
                        </div>
                    </div>
                    <div class="card-footer text-right ">
                        @if($object->id)
                            <button type="button" class="btn btn-dark btn-file">
                                <i class="fa fa-files"></i> Archivos
                            </button>
                        @endif

                        @if($user->role == 1 or $user->role == 2)
                            <button type="submit" class="btn btn-success btn-save">
                                Guardar <i class="fa fa-save"></i>
                            </button>
                        @endif
                        @if($object->id)
                            @if($user->is_supervisor or $user->role == 1)
                                <a href="#" class="btn btn-success btn-next"
                                   data-url="{{ route('report.register', ['pk' => $object->get_Folio(), 'step' => 2]) }}">
                                    Siguiente <i class="fa fa-angle-right"></i>
                                </a>
                            @endif
                        @endif
                    </div>
                </div>
                {{Form::close()}}
                @include('report.snippets.file_container')
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css"/>
    <script>
        {{--$('#main-table').setDataTable('{{ route('api.report.list') }}');--}}

        $(document).on('click', '.btn-next', function (e) {
            if ($('.btn-save').length) {
                $('#main_form').append($('<input>').attr('type', 'hidden').attr('name', 'next_step').val(true));
                $('.btn-save').trigger('click');
            } else {
                window.location.href = $(this).attr('data-url');
            }

            e.preventDefault();
        });

        $(document).on('click', '.btn-file', function () {
            $('.file-container').slideDown(400);
        });

        $(document).on('click', '.btn-close-file', function () {
            $('.file-container').slideUp(400);
        });

        $("form#dropzone-files").dropzone({
            paramName: 'file',
            url: '{{ route('report.file') }}',
            dictDefaultMessage: 'Arrastra los archivos aquí.',
            //acceptedFiles: "image/*",
        });
    </script>
@endsection


(function ($) {
    $.fn.extend({
        // Inicializamos Datatables
        initDatatable: function (options) {
            AjaxDatatableViewUtils.initialize_table(
                this,
                options,
                {
                    // sDom: 'Rfrtlip',
                    // Extra Options
                    // search_icon_html: '<i class="fa fa-search" style="font-size: 16px"></i>',
                    processing: true,
                    serverSide: true,
                    ordering: true,
                    autoWidth: false,
                    full_row_select: false,
                    scrollX: true,
                    bFilter: true,
                    sFilter: false,
                    sDom: '<"top"i>rt<"bottom"flp><"clear">',
                    language: {
                        zeroRecords: "Información no encontrada",
                        info: "Pagína _PAGE_ de _PAGES_",
                        infoEmpty: "Sin infromación",
                        infoFiltered: "(filtrado de un total de _MAX_ resgitros)",
                        sProcessing: "Procesando espere...",
                        sLoadingRecords: "Cargando...",
                        lengthMenu: "_MENU_",
                        oPaginate: {
                            sFirst: "Primero",
                            sLast: "Último",
                            sNext: "Siguiente",
                            sPrevious: "Anterior"
                        },
                        buttons: {
                            pageLength: {
                                _: "%d registros",
                                '-1': "Todos los registros"
                            },
                            copy: "<i class='la la-copy'></i> Copiar",
                            csv: "<i class='la la-file-text-o'></i> CSV",
                            excel: "<i class='la la-file-excel-o'></i> Excel",
                            pdf: "<i class='la la-file-pdf-o'></i> PDF",
                            print: "<i class='la la-print'></i> Imprimir",
                        }
                    }
                }, {
                    // extra_data
                    // ...
                }
            )
        },

        //Ejecutamos Guardar Formulario
        initBtnSave: function () {
            this.on('click', function () {
                var $this = $(this);
                var main_form = $this.closest('form');
                var url = main_form.attr('action');
                var modal = $('.btn-register').attr('data-bs-target');

                $this.attr('data-kt-indicator', 'on');
                $this.attr('disabled', 'disabled');

                // var data = main_form.serializeArray();
                var data = new FormData(document.getElementById('main_form'));
                console.log(data)
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'html',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $(modal + ' .modal-content').html(response);
                        $('table.dataTable').DataTable().ajax.reload(null, false);
                    }
                }).done(function () {
                    $('.btn-save').initBtnSave();
                    $('.file-selector').initFileSelector();

                    $('#search_form_field').val(' ');
                    $('#search_form_field').trigger('keyup');
                    $('#search_form_field').val('');
                    $('#search_form_field').trigger('keyup');

                    setTimeout(function () {
                        $('.notice-snippet').fadeOut('slow', function () {
                            $('.notice-snippet').remove();
                        });
                    }, 4000);
                }).fail(function (x, y, z) {
                    $this.removeAttr('data-kt-indicator');
                    $this.removeAttr('disabled', 'disabled');
                });
            });
        },

        initFileSelector: function () {
            var $this = this;
            this.find('span').hide();
            var id = this.find('input[type="file"]').attr('id');
            this.append('<label class="btn btn-active-light-primary w-100" for="' + id + '">Selecciona archivo <i class="fa fa-file"></i></label>');

            this.find('input[type="file"]').on('change', function () {
                if ($(this).val() == '') {
                    var original_src = $(this).closest('div').find('img').attr('data-original');

                    $(this).closest('div').find('img').attr('src', original_src);
                } else if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    console.log(reader)
                    reader.onload = function (e) {
                        $this.find('img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        },

        setDataTable: function (url, data_Columns) {
            this.DataTable({
                ajax: url,
                serverSide: true,
                processing: true,
                aaSorting: [[0, "desc"]],
                language: {
                    zeroRecords: "Información no encontrada",
                    info: "Pagína _PAGE_ de _PAGES_",
                    infoEmpty: "Sin infromación",
                    infoFiltered: "(filtrado de un total de _MAX_ resgitros)",
                    sProcessing: "Procesando espere...",
                    sLoadingRecords: "Cargando...",
                    lengthMenu: "_MENU_",
                    oPaginate: {
                        sFirst: "Primero",
                        sLast: "Último",
                        sNext: "Siguiente",
                        sPrevious: "Anterior"
                    },
                    buttons: {
                        pageLength: {
                            _: "%d registros",
                            '-1': "Todos los registros"
                        },
                        copy: "<i class='la la-copy'></i> Copiar",
                        csv: "<i class='la la-file-text-o'></i> CSV",
                        excel: "<i class='la la-file-excel-o'></i> Excel",
                        pdf: "<i class='la la-file-pdf-o'></i> PDF",
                        print: "<i class='la la-print'></i> Imprimir",
                    }
                },
                columns: data_Columns,
                initComplete: function () {
                    $('#main-table_length').appendTo('.card-title-table div');
                    $('#main-table_length select').addClass('form-select form-select-solid form-select-sm');
                }
            });
        }
    });
})(jQuery);

$(document).on('click', '.btn-register', function (e) {
    var url = $(this).attr('data-url');
    var modal = $(this).attr('data-bs-target');

    $(modal + ' .modal-content').html('<div class="text-center fs-3"><br><h2>Cargando <i class="fa fa-spin fa-circle-notch fs-3"></i></h2><br></div>');

    var ajax_event = $.get(url, function (response) {
        $(modal + ' .modal-content').html(response);
    });

    ajax_event.done(function () {
        $('.btn-save').initBtnSave();
        $('.file-selector').initFileSelector();
    });

    ajax_event.fail(function () {
        $(modal + ' .modal-content').html('<div class="text-center fs-3"><br><h2>Ocurrio un error: Intenta de nuevo o contacta a soporte</h2></br></div>')
    });
    e.preventDefault();
});

$(document).on('click', '.data-active', function (e) {
    $this = $(this);
    var data = {
        '_token': $('meta[name="CSRF-Token"]').attr('content'),
    }

    $.post($this.attr('data-url'), data, function () {
        Swal.fire({icon: 'success', text: 'Registro habilitado correctamente.'});
    }).done(function () {
        $('table.dataTable').DataTable().ajax.reload(null, false);
    });
    e.preventDefault();
});

$(document).on('click', '.data-remove', function (e) {
    $this = $(this);
    Swal.fire({
        icon: 'warning',
        title: '¿Estas seguro?',
        html: 'Puedes eliminar el registro u optar por inhabilitarlo.',
        input: 'checkbox',
        inputPlaceholder: 'Eliminar permanentemente',
        showCancelButton: true
    }).then((result) => {
        if (result.isConfirmed) {
            var forceDelete = false;
            if (result.value) {
                forceDelete = true;
            }

            var data = {
                '_token': $('meta[name="CSRF-Token"]').attr('content'),
                'forceDelete': forceDelete
            }

            $.post($this.attr('data-url'), data, function () {
                var data_result = (forceDelete) ? 'eliminado' : 'inhabilitado';
                data_result = 'Registro ' + data_result + ' correctamente.'
                Swal.fire({icon: 'success', text: data_result});
            }).done(function () {
                $('table.dataTable').DataTable().ajax.reload(null, false);
            });
        }
    });
    e.preventDefault();
});

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

$(document).on('keyup', '[data-kt-table-filter="search"]', function () {
    $('table.dataTable').DataTable().search(this.value).draw();
});

var start = moment().subtract('days', 7);
var end = moment();

$('.date_range').daterangepicker({
    opens: 'right',
    startDate: moment().subtract('days', 7),
    endDate: end,
    alwaysShowCalendars: true,
    showDropdowns: true,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Últimos 7 Days': [moment().subtract(6, 'days'), moment()],
        'Últimos 30 Days': [moment().subtract(29, 'days'), moment()],
        'Este mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }, locale: {
        format: 'Y-MM-DD'
    }
});

class ChartFunction {
    static initChartPie(chartSelector, url) {
        var options;
        $.ajax({
            url: url,
            method: 'GET',
            async: false,
            success: function (response) {
                options = {
                    colors: ['#f3772d', '#f05676', '#e4b527', '#00abd2', '#d6188f', '#58bb57', '#f68f6c', '#ee1b2b','#fbc5a0','#fff10b','#35459d','#7f2e92','#765343','#017752'],
                    series: response.series,
                    labels: response.labels,
                    chart: {type: 'pie', toolbar: {show: 1}},
                    stroke: {colors: ['#fff']},
                    fill: {opacity: 0.8},
                    toolbar: {show: 1},
                    responsive: [{
                        breakpoint: 480,
                        options: {
                            chart: {width: 100, height: 100, toolbar: {show: 1}}, legend: {position: 'bottom'}, toolbar: {show: 1}
                        }
                    }]
                };
            }
        });

        var chart_informer_gender = new ApexCharts(document.querySelector(chartSelector), options);
        chart_informer_gender.render();
        return chart_informer_gender;
    }

    static updateChartPie(chartElement, url) {
        $.get(url, function (response) {
            var options = {
                series: response.series,
                labels: response.labels,
            };
            chartElement.updateOptions(options);
        });
    }

    static initChart(chartSelector, url) {
        var chartElement;
        var options;
        $.ajax({
            url: url,
            method: 'GET',
            async: false,
            success: function (response) {
                options = {
                    colors: ['#E1647C', '#B3AECA', '#4F458F', '#F3F4F4', '#F75EB1', '#E060D1', '#BA5EF7', '#E060D1'],
                    chart: {
                        zoom: {
                            enabled: false
                        }, fontFamily: 'inherit', type: response.type, height: '500px', toolbar: {show: 1},
                    },
                    title: {
                        text: response.title,
                        align: 'left'
                    },
                    plotOptions: {
                        bar: {horizontal: 1, columnWidth: ["40%"], endingShape: 'rounded', borderRadius: 4,}},
                    series: response.series,
                    xaxis: {
                        categories: response.categories,
                        axisBorder: {show: !1},
                        axisTicks: {show: !1},
                        labels: {style: {fontSize: "12px"}}
                    }
                }
            }
        })
        chartElement = new ApexCharts(document.querySelector(chartSelector), options);
        chartElement.render();
        return chartElement;
    }

    static updateChart(chartElement, url) {
        $.get(url, function (response) {
            var options = {
                series: response.series,
                xaxis: {
                    categories: response.categories,
                }
            }
            chartElement.updateOptions(options);
        });
    }
}

